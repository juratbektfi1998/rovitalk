import React from 'react';
import "./App.css";
import { createStore, applyMiddleware, compose } from 'redux';
import { BrowserRouter as Router } from "react-router-dom";
import Routes from "./routes";
import thunkMiddleware from 'redux-thunk';
import rootReducer from "./recux/rootReducer";
import { Provider } from 'react-redux';

const store = createStore(rootReducer, compose(applyMiddleware(thunkMiddleware)));

function App() {
  return (
      <Provider store={store}>
          <Router>
              <Routes />
          </Router>
      </Provider>
  );
}

export default App;
