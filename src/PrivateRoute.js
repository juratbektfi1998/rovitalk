import React from "react";
import { Route, Redirect } from "react-router-dom";
import { useAuth } from "./components/Auth/Auth";

export function PrivateRoute({ children, ...rest }) {
    const token = localStorage.getItem('rovi-authenticationToken');
    let auth = useAuth();
  return (
    <Route
      {...rest}
      render={({ location }) =>
        token ? (
          children
        ) : (
          <Redirect
            to={{
              pathname: "/login",
              state: { from: location },
            }}
          />
        )
      }
    />
  );
}
