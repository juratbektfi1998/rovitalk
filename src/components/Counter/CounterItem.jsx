import React from "react";
import CountUp from "react-countup";
import "./Counter.scss";

function CounterItem(props) {
  const { endCount, countText } = props;

  return (
    <div id="counter" className="section-counter">
      <div className="counter-value mb-2">
        <CountUp start={0} end={endCount} duration={2} useEasing={true} />
      </div>

      <h4 className="text-uppercase font-weight-bolder">{countText}</h4>
    </div>
  );
}

export default CounterItem;
