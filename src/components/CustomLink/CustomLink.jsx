import React from "react";
import { NavLink } from "react-router-dom";

import "./CustomLink.scss";

function CustomLink({_id, title, is_active, is_main, toggle, path}) {
  return (
    <NavLink
      to={(is_main ? `/dashboard/section/${_id}` : path) || ''}
      activeClassName="is-active"
      className="custom-link"
      onClick={toggle}
    >
      {title}
    </NavLink>
  );
}

export default CustomLink;
