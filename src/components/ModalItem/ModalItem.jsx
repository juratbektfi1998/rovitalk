import React, { useState } from "react";

import { Modal, ModalHeader, ModalBody } from "reactstrap";

import "./ModalItem.scss";

function ModalItem(props) {
  const { buttonLabel, className, title, component, colorText, btnClass } = props;
  const [modal, setModal] = useState(false);
  const toggle = () => setModal(!modal);

  return (
    <div>
      <button
        color="danger"
        style={{ color: colorText }}
        className={`btn-label ` + btnClass }
        onClick={toggle}
      >
        {buttonLabel}
      </button>
      <Modal
        isOpen={modal}
        toggle={toggle}
        centered={true}
        className={className}
      >
        <ModalHeader toggle={toggle}> {title} </ModalHeader>
        <ModalBody>{component}</ModalBody>
        {/* <ModalFooter>
          <Button color="primary" onClick={toggle}>
            Do Something
          </Button>{" "}
          <Button color="secondary" onClick={toggle}>
            Cancel
          </Button>
        </ModalFooter> */}
      </Modal>
    </div>
  );
}

export default ModalItem;
