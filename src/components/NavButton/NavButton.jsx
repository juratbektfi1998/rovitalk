import React from "react";
import { useTranslation } from "react-i18next";
import { useHistory } from "react-router-dom";
import { Button } from "reactstrap";
import Login from "../../views/home/Login/Login";
import { useAuth } from "../Auth/Auth";
import ModalItem from "../ModalItem/ModalItem";
import "./NavButton.scss";

function NavButton() {
    const token = localStorage.getItem('rovi-authenticationToken');
  function AuthButton() {
    let history = useHistory();
    let auth = useAuth();
    const { t, i18n } = useTranslation();

    return token ? (
      <button className="nav-last-button"
        onClick={() => {
            localStorage.removeItem('rovi-authenticationToken');
            auth.signout(() => history.push("/"));
        }}
      >
        Sign out
      </button>
    ) : (
      <ModalItem
        buttonLabel={t(`home_navbar_button.title`)}
        title={t(`home_navbar_button.title`)}
        component={<Login />}
        btnClass="nav-last-button"
      />
    );
  }
  return (
    <div>

        <AuthButton />

    </div>
  );
}

export default NavButton;
