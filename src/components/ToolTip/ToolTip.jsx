import React, { useState } from "react";
import { Tooltip } from "reactstrap";

function ToolTip(props) {
  const [tooltipOpen, setTooltipOpen] = useState(false);

  const toggle = () => setTooltipOpen(!tooltipOpen);
  return (
    <Tooltip
      placement={props.orentation}
      isOpen={tooltipOpen}
      target={props.customId}
      toggle={toggle}
    >
      {props.text} <br />
      {props.textUnder}
    </Tooltip>
  );
}

export default ToolTip;
