import axios from 'axios';
import {AXIOS_TIMEOUT, SERVER_API_URL} from './constants';

const token = localStorage.getItem('rovi-authenticationToken');
const language = localStorage.getItem('i18nextLng') || 'ru';

export default axios.create({
    baseURL: SERVER_API_URL,
    timeout: AXIOS_TIMEOUT,
    headers: {
        role: "user",
        language: language,
        Authorization: token || null
    }
});

const onRequestSuccess = config => {
    const token = localStorage.getItem('rovi-authenticationToken');
    if (token) {
        config.headers.Authorization = `Bearer ${token}`;
    }
    return config;
};

export const logOut = () => {
    localStorage.removeItem('rovi-authenticationToken');
};

const onResponseSuccess = response => response;
const onResponseError = err => {
    const status = err.status || err.response.status;
    if (status === 401 || status === 403) {
        logOut();
        window.location = '/'
    }
    alert(err);
    return Promise.reject(err);
};
axios.interceptors.request.use(onRequestSuccess);
axios.interceptors.response.use(onResponseSuccess, onResponseError);

