import React, {useState, useEffect} from "react";
import {Link, NavLink} from "react-router-dom";
import {
    Collapse,
    DropdownItem,
    DropdownMenu,
    DropdownToggle,
    Nav,
    Navbar,
    NavbarBrand,
    NavbarToggler,
    NavItem,
    UncontrolledDropdown,
} from "reactstrap";
import ToolTip from "../../../components/ToolTip/ToolTip";
import "./DashNavBar.scss";
import { logOut } from "../../../config/axios-interceptor";
import axios from '../../../config/axios-interceptor';

function DashNavbar({userName = "UserName"}) {
    const [isOpen, setIsOpen] = useState(false);
    const [items, setItems] = useState([]);

    useEffect(() => {
        axios
            .post("sections")
            .then((res) => setItems(res.data.data))
            .catch((err) => alert(err));
    }, []);

    const toggle = () => setIsOpen(!isOpen);
    return (
        <div className="dash-topnavbar">
            <Navbar
                color="light"
                light
                className="bg-white border-bottom"
                fixed="top"
                expand="md"
            >
                <NavbarBrand>
                    {" "}
                    <Link id="TooltipBrand" to={`/main`}>
                        <img
                            style={{width: "100px", height: "auto"}}
                            src="http://rovitalk.com/wp-content/uploads/2019/10/logo_big_2x.png"
                            alt="RoviTalk – Ertakterapiya, audiokitoblar, psixologik mashqlar va ajdodlar oltin merosi."
                        />
                    </Link>
                    <ToolTip
                        orentation="bottom"
                        customId="TooltipBrand"
                        text="RoviTalk – Ertakterapiya, audiokitoblar, psixologik mashqlar va ajdodlar oltin merosi."
                    />
                </NavbarBrand>
                <NavbarToggler onClick={toggle}/>
                <Collapse isOpen={isOpen} navbar>
                    <Nav className="mr-auto dash-nav-top" navbar>
                        {items.length > 0 && items.map((item, key)=>(
                            <NavItem>
                                <NavLink
                                    onClick={toggle}
                                    to={item.is_active ? `/dashboard/section/${item._id}` : '/dashboard/tariflar'}
                                    className="custom-link-dash-nav "
                                    activeClassName="is-active-dash-nav"
                                >
                                    {item.title}
                                </NavLink>
                            </NavItem>
                        ))}
                    </Nav>
                    <Nav className="ml-auto right-user">
                        <UncontrolledDropdown nav inNavbar>
                            <DropdownToggle nav caret className="text-muted">
                                <i className="fas fa-user-circle mr-2"/> {userName}
                            </DropdownToggle>
                            <DropdownMenu right>
                                <DropdownItem>
                                    {" "}
                                    <Link to="/dashboard/userprofil">
                                        {" "}
                                        <span className="icon-User"/> UserProfil{" "}
                                    </Link>{" "}
                                </DropdownItem>
                                <DropdownItem>
                                    <Link to="/dashboard/editprofil">
                                        {" "}
                                        <span className="icon-Gears"/> Edit profil{" "}
                                    </Link>
                                </DropdownItem>
                                <DropdownItem>
                                    <Link to="/dashboard/tarifolish">
                                        {" "}
                                        <span className="icon-Credit-Card2"/> Tarif Olish{" "}
                                    </Link>
                                </DropdownItem>
                                <DropdownItem divider/>
                                <DropdownItem>
                                    <Link to="/main" onClick={() => logOut()} className="log-out-link">
                                        {" "}
                                        <span className="icon-Power-3"/> Chiqish{" "}
                                    </Link>
                                </DropdownItem>
                            </DropdownMenu>
                        </UncontrolledDropdown>
                    </Nav>
                </Collapse>
            </Navbar>
        </div>
    );

}

export default DashNavbar;
