import React, {useEffect, useState} from "react";
import {NavLink, useHistory} from "react-router-dom";

import "./DashSideBar.scss";
import axios from "../../../config/axios-interceptor";
import SideBarIcon from "./SideBarIcon/SideBarIcon";
import ImgTop from "./шш.png";

function DashSideBar() {
    const [navItems, setNavItems] = useState([]);
    const history = useHistory();

    useEffect(() => {
        axios
            .post("sections")
            .then((res) => setNavItems(res.data.data))
            .catch((err) => alert(err));
    }, []);

    const redirect = () => {
        console.log('redirect1');
        history.push('/dashboard/tarifolish')
    };

    return (
        <div className="dash-sidebar">
            {navItems.length > 0 &&
            navItems.map((item, key) => item.is_active ? (
                <NavLink
                    key={key}
                    activeClassName="is-active-dash"
                    className={`custom-link-dash`}
                    to={`/dashboard/section/${item._id}`}
                >
                    {" "}
                    <SideBarIcon iconUrl={item.image ? item.image : ImgTop}/>
                    {item.title}
                </NavLink>
            ) : (
                <div className="active-qulf" onClick={redirect}>
                    <div className="qulf">
                        <i className="fas fa-lock"/>
                    </div>
                    <NavLink
                        activeClassName="is-active-dash"
                        className="custom-link-dash"
                        to="/dashboard/tarifolish"
                    >
                        <SideBarIcon iconUrl={item.image ? item.image : ImgTop}/>
                        {item.title}
                    </NavLink>
                </div>
            ))}

        </div>
    );
}

export default DashSideBar;
