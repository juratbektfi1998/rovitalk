import React from "react";

import "./SideBarIcon.scss";

function SideBarIcon({
  iconUrl = "https://scontent-yyz1-1.cdninstagram.com/v/t51.2885-15/e15/11282657_1392522974409783_1149554758_n.jpg?_nc_ht=scontent-yyz1-1.cdninstagram.com&_nc_cat=103&_nc_ohc=uP8XKIQpe58AX8RyxN4&tp=18&oh=36b0ad7580568e0133f895171d2effc8&oe=5FD35117",
}) {
  return (
    <div
      className="iconsidebar-img"
      style={{ backgroundImage: `url(${iconUrl})` }}
    />
  );
}

export default SideBarIcon;
