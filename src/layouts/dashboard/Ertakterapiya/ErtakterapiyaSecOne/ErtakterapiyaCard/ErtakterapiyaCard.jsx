import React, {} from "react";
import {Link} from "react-router-dom";
import "./ErtakterapiyaCard.scss";
import {useHistory} from "react-router-dom";

function ErtakterapiyaCard({id, imgUrl, countBook, title,sectionTitle}) {

    const history = useHistory();

    const handleClick = (id) => {
        history.push(`/dashboard/file/${id}`)
    }

    return (
        <div className="ertakterapiya-card mb-3" onClick={() => handleClick(id)}>
                <img src={imgUrl}/>
            <h5 className="w-100 text-center my-2"> {countBook} ta ertak</h5>
        </div>
    );
}

export default ErtakterapiyaCard;
