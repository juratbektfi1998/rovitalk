import React, {useEffect, usePath, useState, useRef} from "react";
import {Col, Row} from "reactstrap";
import ErtakterapiyaCard from "./ErtakterapiyaCard/ErtakterapiyaCard";

import "./ErtakterapiyaSecOne.scss";
import axios from "../../../../config/axios-interceptor";


function ErtakterapiyaSecOne() {

    //
    // const hasMount = useRef(false)
    const [category, setCategory] = useState([]);
    useEffect(() => {
        run()
    }, [window.location.pathname]);

    function run() {
        const arr = window.location.href.split('/');
        if (arr.length === 6)
            if (arr[4] === 'section') {
                axios.post(`category/${arr[5]}`)
                    .then(res => setCategory(res.data.data))
                    .catch(err => alert(err))
            }

    }

    return (
        <div className="ertakterapiya-sec-one mb-5">
            <Row className="flex-wrap">
                {category.map((item, index) => (
                    <Col sm="6" md="4" lg="2">
                        <ErtakterapiyaCard
                            id={item._id}
                            imgUrl={item.image}
                            countBook={item.count}
                            title={item.title}
                            sectionTitle={item.section_title}
                            key={index}
                        />
                    </Col>
                ))}
            </Row>
        </div>
    );
}

export default ErtakterapiyaSecOne;
