import React from "react";
import { Col, Row } from "reactstrap";
import "./ErtakterapiyaSecTwo.scss";

function ErtakterapiyaSecTwo() {
  return (
    <div className="ertakterapiya-sec-two mb-5">
      <div className="section-one mb-3">
        <h3 className="font-weight-bold w-100 text-center mb-3">
          {" "}
          Ertaklar farzandlarimizga qanday ta'sir ko'rsatadi?{" "}
        </h3>
        <Row>
          <Col sm="12" md="8">
            <div className="left-box">
              <p className="text-muted">
                Bolalar juda qiziquvchan bo’lishadi. Atrofdagi voqea
                hodisalarning sababini, natijasini bilgisi keladi, biror bir
                holat yuzasidan munosabatingizga qiziqadi, ba’zan shunaqangi
                ko’p savol berishadiki, qay biriga javob berishni bilmay qolasan
                kishi. Bunday paytda bolaga to’g’ri ma’lumot berish juda muhim!
                Ammo biz, kattalar doim ham farzandimizning yonida bo’la
                olmaymiz. Bolani esa kichkinaligidan mustaqil qaror qabul
                qilishi, fikr bildirishiga o’rgatib borish lozim.
              </p>
              <p className="text-muted">
                Ertaklar esa bolada ana shu hususiyatlarni rivojlantirishga
                yordam beradi. Ayniqsa, bugun dunyo psihologlari amalyotida keng
                qo’llanilayotgan ertakterapiya nafaqat kerakli ma’lumotni
                yetkazadi, balki davolovchi ta’sirga ega ekanligi bilan ham
                yuqori bahonalmoqda.
              </p>
            </div>
          </Col>
          <Col sm="12" md="4">
            <div className="right-box">
              <img
                src="http://rovitalk.com/wp-content/uploads/2019/10/img2.png"
                alt="right-img"
              />
            </div>
          </Col>
        </Row>
      </div>

      <div className="section-two mb-3">
        <h3 className="font-weight-bold w-100 text-center mb-3">
          {" "}
          Ho'sh, ertakterapiya nima o'zi?{" "}
        </h3>
        <p className="text-muted">
          Bu ertaklar yordamida davolash uslubi hisoblanadi. U nafaqat
          kasalliklar, balki fobiyalar, salbiy odatlar, turli hil tushunchalarni
          o’zgartirishda ham yahshi ta’sir ko’rsatadi.
        </p>
        <p className="text-muted">
          Bu juda sodda tilda yozilgan psihologik ertaklar hisoblanadi. Uni
          eshitgan yoki o’qigan har qanday bola o’zidagi mavjud mummoni anglaydi
          va undan chiqish yo’li qanday ekanligini tushunadi. Ertakterapiya
          hatto eng kuchli nevroz va fobiyalarda ham juda ham ajoyib, ijobiy
          natijalar ko’rsatadi. Bunday ertaklar yordamida qisqa vaqt ichida
          bolada kuzatilayotgan har qanday muammoni yechimini topish mumkin.
        </p>
        <p className="text-muted">
          Sizning farzandlaringizni salomatligi, hulq-atvori, xarakteri, ba’zi
          tushunchalari o’ylantirayotgan bo’lsa, hech ikkilanmay ertakterapiyaga
          murojaat eting. Xotirjam bo’ling, natijasi sizni judayam quvontiradi.
        </p>
      </div>

      <div className="section-three">
        <h3 className="font-weight-bold w-100 text-center mb-3">
          {" "}
          Xarxashalarga chek qo'yish vaqti keldi...{" "}
        </h3>
        <p className="text-muted">
          Biz, ota-onalarning vazifamiz farzandlarimizni oʼqitish, ularga
          tarbiya berish bilangina kifoyalanmaydi. Bolaga yaxshi xulq-atvor,
          goʼzal insoniy fazilatlarni singdirish, qiziqishlariga koʼra toʼgʼri
          yoʼnaltirish va uni harakatga undashga ham biz masʼulmiz. Ammo bu
          jarayonlarda bolaga baqirish, urishish, jazolash, turli ta’qiqlar
          qoʼyish va shu kabi boshqa keraksiz choralarni qoʼllab boʼlmaydi.
          Maʼlumotni bolaga tushunarli tarzda yetkazish lozim, masalan,
          ertakterapiya yordamida.
        </p>
      </div>

      <div className="section-four">
        <h3 className="font-weight-bold w-100 text-center mb-3">
          {" "}
          Ertakterapiyaning davolovchi ta'siri rostmi?{" "}
        </h3>

        <p className="text-muted">
          Ertakterapiya – bu psixoterapiya yoʼnalishi boʼlib, u xulq-atvor
          reaktsiyasini tuzatish hamda boshqarib boʼlmaydigan qoʼrquvni
          oʼrganish maqsadida tavsiflanadigan psixologik aralashuvdir.
          Ertakterapiyaning afzalligi, u psixoterapevt ishtirokisiz, asosiy
          usullarni oʼzlashtirgan holda bolaning xatti-harakatlariga bosim
          oʼtkazmay taʼsir koʼrsatish imkonini beradi.
        </p>

        <Row>
          <Col sm="12" md="4">
            <div className="mb-3">
              <img
                src="http://rovitalk.com/wp-content/uploads/2019/10/1_111_1.png"
                alt="left-img"
              />
            </div>
          </Col>
          <Col sm="12" md="8">
            <div className="mb-3">
              <p className="text-muted mb-2">
                Sehrli taʼsirga ega ertaklar quyidagi muammolarni bartaraf
                etadi:
              </p>
              <ul>
                <li>
                  <p className="text-muted">
                    <b>Bola xarxashalari</b> – beparvolik, aldamchilik,
                    dangasalik, agressiya, gigiyena qoidalariga rioya etish,
                    bogʼchaga borish, kun tartibiga amal qilishni istmaslik.
                  </p>
                </li>
                <li>
                  {" "}
                  <p className="text-muted">
                    <b>Fobiyalar</b> – ota-onadan ajralib qolish, qorong’ulik,
                    hayvonlar, shifokordan qoʼrqish.
                  </p>{" "}
                </li>
                <li>
                  <p className="text-muted">
                    {" "}
                    <b>Kechinmalar</b>– boshqa bolalar bilan chiqisha olmaslik,
                    yaqin odamini yoʼqotish, yangi oila aʼzosining paydo
                    boʼlishi.{" "}
                  </p>
                </li>
              </ul>

              <p className="text-muted">
                {" "}
                Boladagi qoʼrquv, kuchli xavotir va ichki kechinmalarni
                tushunish qiyin boʼlganida ota-onalarga mutaxassis koʼmagi zarur
                boʼlishi mumkin. Bunday hamkorlik faqat alomatlar bilangina
                kurashmay, balki muammoning asl sababini topish va uni bartaraf
                etish imkonini beradi.{" "}
              </p>
            </div>
          </Col>
        </Row>
      </div>

      <h3 className="font-weight-bold w-100 text-center mb-3">
        {" "}
        Ertaklar bolaga qanday yordam beradi?{" "}
      </h3>

      <p className="text-muted">
        Biz ertaklarni sehrli deyapmiz. Xoʼsh, uning sehri nimada? Bolaga qanday
        taʼsir koʼrsatadi? Ertakterapiya bolajonlar uchun zerikarli nasihatlaru
        turli ko’rsatmalardan koʼra qiziqarliroq va tushunarliroq. U bola ichki
        dunyosiga kirish va unga taʼsir koʼrsatish, vaziyatni toʼgʼrilashga
        yordam beradi. Shuningdek, bunday holat faqatgina u bilan sodir
        boʼlmayotganini tushuntirishga koʼmaklashadi.
      </p>
      <br />
      <p className="text-muted">
        Reallik va ertaknamo voqealarni tushunish chegarasi bola ongostida
        yuvilib ketgani bois u ertak qahramonlari uchun chin dildan qaygʼuradi,
        ularning kechinmalarini his etadi, boshdan oʼtkazadi, yuragiga yaqin
        olib, tahlil qiladi.
      </p>

      <Row>
        <Col sm="12" md="4">
          <div>
            <img
              src="http://rovitalk.com/wp-content/uploads/2019/10/photo_11.png"
              alt="one"
            />
            <h3 className="w-100 text-center text-uppercase text-success font-weight-bold my-2">
              {" "}
              muammo{" "}
            </h3>
            <p className="text-center w-100 text-muted">
              Farzandingiz injiqlik qilyaptimi? Yoki muammolari bormi?
            </p>
          </div>
        </Col>
        <Col sm="12" md="4">
          <div>
            <img
              src="http://rovitalk.com/wp-content/uploads/2019/10/photo_21.png"
              alt="one"
            />
            <h3 className="w-100 text-center text-uppercase text-success font-weight-bold my-2">
              {" "}
              ertakterapiya{" "}
            </h3>
            <p className="text-center w-100 text-muted">
              Farzandingiz injiqlik qilyaptimi? Yoki muammolari bormi?
            </p>
          </div>
        </Col>
        <Col sm="12" md="4">
          <div>
            <img
              src="http://rovitalk.com/wp-content/uploads/2019/10/photo_31.png"
              alt="one"
            />
            <h3 className="w-100 text-center text-uppercase text-success font-weight-bold my-2">
              {" "}
              natija{" "}
            </h3>
            <p className="text-center w-100 text-muted">
              Farzandingiz injiqlik qilyaptimi? Yoki muammolari bormi?
            </p>
          </div>
        </Col>
      </Row>

      <h3 className="font-weight-bold w-100 text-center mb-3">
        {" "}
        Bolalar yoshiga ko'ra ertakterpiya turlari{" "}
      </h3>

      <p className="text-muted">
        <b>Ikki yoshga</b> toʼlmagan bolakay hali sehrli ertaklar maʼnosini
        tahlil qilolmaydi. U ongostiga yaxshi xulq-atvor, yuzaga kelishi mumkin
        boʼlgan xavflar toʼgʼrisidagi maʼlumotlarni joylashi kerak boʼlgan
        sabab-oqibatlarni idrok etishga qodir emas. Ikki yoshdan keyin esa oddiy
        terapevtik ertaklardan foydalansa boʼladi. Ertak injiq, hadeb xarxasha
        qiladigan, uxlashni istamaydigan bolani tinchlantiradi, mustaqil
        ovqatlanish koʼnikmalarini paydo qiladi va xotirasini rivojlantiradi.
      </p>
      <br />
      <p className="text-muted">
        <b>Uch yoshdan</b> keyin ertakterapiyaga koʼproq murojaat etgan yaxshi.
        Ayniqsa, odamlarning jonivorlarga munosabati haqidagi ertaklar bolalar
        uchun gʼoyat foydali. Bu yoshda aksariyat bolakaylar oʼzlarini
        jonivorlarga oʼxshatishadi va ularning xatti-harakatlarini oʼzlariniki
        bilan solishtirishadi.
      </p>
      <br />
      <p className="text-muted">
        <b>Farzandingiz maktab yoshida</b> boʼlsa, ichki dunyosini tushunish,
        yaqinlari va doʼstlari bilan muammolarni hal etishga yordam beruvchi
        masal va kichik hikoyalarni qoʼllab koʼrishingiz mumkin. Shuningdek,
        ertakterapiyada qissa va she’rlardan ham foydalaniladi. Bunday hikoyalar
        hazil va istehzolarga murojaat etilishi bilan ham ajoyib ham qiziqarli.
      </p>
      <br />
      <p className="text-muted">
        Turli xil komplekslari bor, oʼziga bahosi past, ota-ona eʼtibori,
        mehridan mahrum boʼlgan maktabgacha va yuqori sinf oʼquvchilariga
        moʼjizalar, mistik hodisalar va gʼayrioddiy oʼzgarishlar yuz beradigan
        sehrli hikoyalar oʼqish tavsiya etiladi. Ularning yordami bilan bola
        ongosti har bir hikoya, ertakda tasvirlangan hayotiy donolik bilan
        toʼlib boradi.
      </p>

      <h3 className="font-weight-bold w-100 text-center mb-3">
        {" "}
        Ertakterapiya bilan qanday natijalarga erishish mumkin?{" "}
      </h3>

      <Row>
        <Col sm="12" md="3">
          <div>
            <img
              src="http://rovitalk.com/wp-content/uploads/2019/10/2222.png"
              alt="left-data"
            />
          </div>
        </Col>
        <Col sm="12" md="9">
          <div>
            <p className="text-muted">
              Mazkur usulning afzalligi shundaki, farzandingiz bilan
              shugʼullanish uchun sizdan maxsus bilim talab etilmaydi. Siz
              faqatgina kerakli ertakni dasturdan farzandingizga qo’yib
              bersangiz bas. Ammo shunday boʼlsa-da, ertakterapiya yordamida
              qaysi muammoni hal etish va qanday natijaga ega boʼlishni
              istayotganingizni aniq bilishingiz kerak. Agar bu borada bir
              qarorga kela olmayotgan boʼlsangiz diqqat qiling. Quyida
              ertakterapiya bolaga qanday yordam berishiga toʼxtalamiz:
            </p>
            <p className="text-muted">
              Bolakayni <strong> qorongʼilik </strong>,
              <strong> balandlik </strong>, <strong> suvda </strong>
              <strong> suzish </strong>,{" "}
              <strong> shifokor koʼrigiga borish </strong>
              kabi qoʼrquvlardan saqlash.
              <br />
              Keyinchalik bolaning kundalik hayotini yorqin va&nbsp;
              <strong>qiziqarli mashgʼulotlar</strong>&nbsp;bilan
              toʼldiradigan&nbsp;
              <strong>ijodiy qobiliyatni rivojlantirish</strong>.<br />
              Bolaga nima yaxshi-yu nima yomonligini oʼrgatish. Masalan,&nbsp;
              <strong>oʼzidan kichkinalarni xafa qilmaslik</strong>,&nbsp;
              <strong>
                birovning oʼyinchoqlarini soʼramasdan olib boʼlmasligi
              </strong>
              ,&nbsp;<strong>yolgʼon gapirmaslik</strong>.<br />
              <strong>Ovqatlanish payti oʼzini yaxshi tutmaslik</strong>
              ,&nbsp;<strong>uy devorlariga chizish</strong>,&nbsp;
              <strong>oʼrnini hoʼllab qoʼyish</strong>&nbsp;kabi fiziologik
              muammolarni bartaraf etish.
              <br />
              Yaqin kishisi,&nbsp;
              <strong>
                oila aʼzolaridan biri yoki uy hayvonidan ajralib qolish
              </strong>
              &nbsp;bilan bogʼliq kechinmalarning yechimini topish.
              <br />
              <strong>Agressiya</strong>&nbsp;va&nbsp;
              <strong>giperfaollikni</strong>&nbsp;yengish.
              <br />
              Bolaga&nbsp;<strong>adolatsizdek</strong>&nbsp;koʼringan
              voqea-hodisalarni&nbsp;<strong>toʼgʼri ifodalash</strong>.
              Masalan,&nbsp;
              <strong>doʼstiga ota-onasi kuchukcha olib berishdi</strong>, ammo
              nega unga ham olib berishmaydi.
              <br />
              <strong>Bolani katta hayotga tayyorlash</strong>.&nbsp;
              <strong>
                Toʼgʼri va mustaqil qaror qabul qilishni oʼrgatish
              </strong>
              .
            </p>
          </div>
        </Col>
      </Row>

      <h3 className="font-weight-bold w-100 text-center mb-3">
        {" "}
        Davolovchi ertaklarni ishlatish uchun tavsiyalar{" "}
      </h3>

      <p className="text-muted">
        Eng muhimi, siz o’z farzandingiz muammosini aniq bilishingiz lozim.
        Bunda ertak qahramoni farzandingizga juda oʼxshash boʼlib, xuddi
        uningdek quloqsiz, shoshqaloq yoki oʼylamay ish qiladigan bola boʼlishi
        mumkin. Misol uchun, uxlashga yotishdan oldin tishlarini tozalashni
        xohlamaydigan bolaga hecham oʼziga eʼtibor berishni istamaydigan,
        kir-chir yuradigan erinchoq qizcha haqida ertak aytib berish mumkin.
        Qizaloqning kir-chir yurishini bilgan Qorbobo Yangi yilda unga sovgʼa
        bermaydi. Bolaga ertakni qiziqarli tarzda soʼzlab berish bizning
        dasturimiz zimmasiga yuklatilgan. Sizni bu ma’suliyatdan ozod qilganmiz.
        Ertak tugagandan keyin, undagi qahramonlar xatti-harakatlarini ham birga
        muhokama qilish, gaplashish maqsadga muvofiq. Shunda bola ertakning asl
        mazmun-mohiyatini tushunadi.
      </p>
      <br />
      <p>
        Eng muhimi, siz o’z farzandingiz muammosini aniq bilishingiz lozim.
        Bunda ertak qahramoni farzandingizga juda oʼxshash boʼlib, xuddi
        uningdek quloqsiz, shoshqaloq yoki oʼylamay ish qiladigan bola boʼlishi
        mumkin. Misol uchun, uxlashga yotishdan oldin tishlarini tozalashni
        xohlamaydigan bolaga hecham oʼziga eʼtibor berishni istamaydigan,
        kir-chir yuradigan erinchoq qizcha haqida ertak aytib berish mumkin.
        Qizaloqning kir-chir yurishini bilgan Qorbobo Yangi yilda unga sovgʼa
        bermaydi. Bolaga ertakni qiziqarli tarzda soʼzlab berish bizning
        dasturimiz zimmasiga yuklatilgan. Sizni bu ma’suliyatdan ozod qilganmiz.
        Ertak tugagandan keyin, undagi qahramonlar xatti-harakatlarini ham birga
        muhokama qilish, gaplashish maqsadga muvofiq. Shunda bola ertakning asl
        mazmun-mohiyatini tushunadi.
        <br />
        <br />
        Xulosa qilib aytganda ertakterapiya tajriba almashish va nima yomon-u,
        nima yaxshi ekanini tushunishning ideal usulidir. Ibratli hikoyalar
        yordamida boladagi&nbsp;<strong>agressiya</strong>,&nbsp;
        <strong>fobiyalar</strong>&nbsp;va&nbsp;
        <strong>nutq muammolarini</strong>&nbsp;bartaraf etish mumkin. Ammo
        ertak terapiyani boshlashdan oldin bola haqiqiy dunyoni xayoldagisidan
        ajrata oladimi, yoʼqmi bilish zarur. Buning uchun ertak terapiyadan
        oldin, farzandingiz bilan ozroq suhbatlashing. Aynan mazkur usul bolada
        oʼziga boʼlgan ishonchni oshirish, jamiyatda&nbsp;
        <strong>oʼzini erkin</strong>,&nbsp;<strong>dadil tutishi</strong>
        &nbsp;va&nbsp;<strong>sogʼlom</strong>,&nbsp;<strong>chiroyli</strong>
        &nbsp;<strong>kelajak</strong>&nbsp;barpo etishida yordam beradi.
      </p>
    </div>
  );
}

export default ErtakterapiyaSecTwo;
