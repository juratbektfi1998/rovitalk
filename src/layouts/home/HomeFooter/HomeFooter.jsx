import React from "react";
import { useTranslation } from "react-i18next";
import { Link } from "react-router-dom";
import { Container } from "reactstrap";
import "./HomeFooter.scss";

function HomeFooter() {
  const { t, i18n } = useTranslation();
  return (
    <div className="home-footer mt-3">
      <Container>
        <ul className="footer-items">
          <li>© 2019 {t(`home_card_footer.Titul`)}</li>
          <li className="other-links">
            {" "}
            <Link to="/securtypage">{t(`home_card_footer.Titul2`)}</Link>{" "}
          </li>
          <li className="other-links">
            {" "}
            <Link to="/rules">{t(`home_card_footer.Titul3`)}</Link>
          </li>
          <li className="other-links">
            {" "}
            <Link>{t(`home_card_footer.Titul4`)}</Link>​
          </li>
          <li className="sot-links">
            {" "}
            <a href="#!">
              <i class="fab fa-youtube"></i>
            </a>
            <a href="#!">
              <i class="fab fa-instagram"></i>
            </a>
            <a href="#!">
              <i class="fab fa-telegram-plane"></i>
            </a>
          </li>
        </ul>
      </Container>
    </div>
  );
}

export default HomeFooter;
