import React, { useState, useEffect } from "react";
import { useTranslation } from "react-i18next";
import { Link } from "react-router-dom";
import axios from '../../../config/axios-interceptor';
import { useDispatch, useSelector } from 'react-redux';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  Nav,
  NavItem,
  NavbarText,
  Container,
  NavLink,
  Button,
} from "reactstrap";
import CustomLink from "../../../components/CustomLink/CustomLink";

import NavButton from "../../../components/NavButton/NavButton";
import ToolTip from "../../../components/ToolTip/ToolTip";

import "./HomeNavBar.scss";
import {getNavItems} from "../../../recux/actions/navbar";
import {navbarReducer} from "../../../recux/reducers/navbar.reducer";

const HomeNavbar = (props) => {
  const [isOpen, setIsOpen] = useState(false);
  const dispatch = useDispatch();
  const navItems = useSelector(store => store.navbarReducer.items).filter(item => item.is_main === true);

  const { t, i18n } = useTranslation();

  const toggle = () => setIsOpen(!isOpen);

  const fakeNavItems = [
    {
      path: "/main",
      title: t(`home_navbar.asosiy`),
    },
    {
      path: "/tariflar",
      title: t(`home_navbar.trafiklar`),
    }
  ];

  useEffect(() => {
    dispatch(getNavItems())
  }, []);

  return (
    <div className="home_top_navbar">
      <Navbar
        color="light"
        light
        expand="lg"
        fixed="top"
        className="w-100 shadow-sm"
      >
        <Container>
          <Link id="TooltipBrand" to={`/main`}>
            <img
              src="http://rovitalk.com/wp-content/uploads/2019/10/logo_big_2x.png"
              alt="RoviTalk – Ertakterapiya, audiokitoblar, psixologik mashqlar va ajdodlar oltin merosi."
            />
          </Link>
          <ToolTip
            orentation="bottom"
            customId="TooltipBrand"
            text={t(`home_toolTip.Titul`)}
          />
          <NavbarToggler onClick={toggle} />
          <Collapse isOpen={isOpen} navbar>
            <Nav className="m-auto" navbar>
              { fakeNavItems.map(item => (
                  <NavItem onClick={toggle}>
                    <CustomLink {...item}/>
                  </NavItem>
              ))}
              {navItems.length > 0 && navItems.map((item) => (
                <NavItem>
                  <CustomLink {...item} />
                </NavItem>
              ))}
              <NavItem className="d-flex justify-content-center">
                {" "}
                <NavLink
                  onClick={() => {
                    i18n.changeLanguage("uz");
                    window.location.reload()
                  }}
                >
                  <button className=" btn btn-success px-2 py-1 mr-1">
                    uz
                  </button>
                </NavLink>{" "}
                <NavLink
                  onClick={() => {
                    i18n.changeLanguage("ru");
                    window.location.reload()
                  }}
                >
                  <button className=" btn btn-success px-2 py-1">ru</button>
                </NavLink>{" "}
              </NavItem>
 
            </Nav>

            <NavbarText>
              <NavButton />
            </NavbarText>
          </Collapse>
        </Container>
      </Navbar>
    </div>
  );
};

export default HomeNavbar;
