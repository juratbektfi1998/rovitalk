import React from "react";
import { useTranslation } from "react-i18next";
import Slider from "react-slick";
import "./MainPageHeaderSlider.scss";
import SlideItem from "./SlideItem/SlideItem";

function MainPageHeaderSlider() {
  const settings = {
    dots: true,
    fade: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 3000,
    pauseOnHover: true,
  };

  const { t, i18n } = useTranslation();

  const slideData = [
    {
      imgUrl: "http://rovitalk.com/wp-content/uploads/2019/10/slide_21.jpg",
      slideTitul: t(`home_main_Slider.slideTitul`),
      slideSubtitul: t(`home_main_Slider.slideSubtitul`),
      slideUnderSubtitul: t(`home_main_Slider.slideUnderSubtitul`),
      slideBtnPath: "/dashboard/section",
      slideBtnText: t(`home_main_Slider.slideBtnText`),
    },
    {
      imgUrl: "http://rovitalk.com/wp-content/uploads/2019/10/slide_44.jpg",
      slideTitul: t(`home_main_Slider.slideTitul2`),
      slideSubtitul: t(`home_main_Slider.slideSubtitul2`),
      slideUnderSubtitul: t(`home_main_Slider.slideUnderSubtitul`),
      slideBtnPath: "/dashboard/section",
      slideBtnText: t(`home_main_Slider.slideBtnText2`),
    },
    {
      imgUrl: "http://rovitalk.com/wp-content/uploads/2019/10/uyqu.jpg",
      slideTitul: t(`home_main_Slider.slideTitul3`),
      slideSubtitul: t(`home_main_Slider.slideSubtitul3`),
      slideUnderSubtitul: t(`home_main_Slider.slideUnderSubtitul3`),
      slideBtnPath: "/dashboard/section",
      slideBtnText: t(`home_main_Slider.slideBtnText3`),
    },
    {
      imgUrl: "http://rovitalk.com/wp-content/uploads/2019/10/slide_32.jpg",
      slideTitul: t(`home_main_Slider.slideTitul4`),
      slideSubtitul: t(`home_main_Slider.slideSubtitul4`),
      slideUnderSubtitul: t(`home_main_Slider.slideUnderSubtitul4`),
      slideBtnPath: "/dashboard/section",
      slideBtnText: t(`home_main_Slider.slideBtnText4`),
    },
    {
      imgUrl: "http://rovitalk.com/wp-content/uploads/2019/10/slide_22.jpg",
      slideTitul: t(`home_main_Slider.slideTitul5`),
      slideSubtitul: t(`home_main_Slider.slideSubtitul5`),
      slideUnderSubtitul: t(`home_main_Slider.slideUnderSubtitul5`),
      slideBtnPath: "/dashboard/section",
      slideBtnText: t(`home_main_Slider.slideBtnText5`),
    },
    {
      imgUrl: "http://rovitalk.com/wp-content/uploads/2019/10/tarbiya1.jpg",
      slideTitul: t(`home_main_Slider.slideTitul6`),
      slideSubtitul: t(`home_main_Slider.slideSubtitul6`),
      slideUnderSubtitul: t(`home_main_Slider.slideUnderSubtitul6`),
      slideBtnPath: "/dashboard/section",
      slideBtnText: t(`home_main_Slider.slideBtnText6`),
    },
    {
      imgUrl: "http://rovitalk.com/wp-content/uploads/2019/10/slide_33.jpg",
      slideTitul: t(`home_main_Slider.slideTitul7`),
      slideSubtitul: t(`home_main_Slider.slideSubtitul7`),
      slideUnderSubtitul: t(`home_main_Slider.slideUnderSubtitul7`),
      slideBtnPath: "/dashboard/section",
      slideBtnText: t(`home_main_Slider.slideBtnText7`),
    },
    {
      imgUrl: "http://rovitalk.com/wp-content/uploads/2019/10/slide_43-1.jpg",
      slideTitul: t(`home_main_Slider.slideTitul8`),
      slideSubtitul: t(`home_main_Slider.slideSubtitul7`),
      slideUnderSubtitul: t(`home_main_Slider.slideUnderSubtitul8`),
      slideBtnPath: "/dashboard/section",
      slideBtnText: t(`home_main_Slider.slideBtnText8`),
    },
  ];

  return (
    <div className="main-page-header">
      <Slider {...settings}>
        {slideData.map((slide) => (
          <SlideItem
            imgUrl={slide.imgUrl}
            slideTitul={slide.slideTitul}
            slideSubtitul={slide.slideSubtitul}
            slideUnderSubtitul={slide.slideUnderSubtitul}
            slideBtnPath={slide.slideBtnPath}
            slideBtnText={slide.slideBtnText}
          />
        ))}
      </Slider>
    </div>
  );
}

export default MainPageHeaderSlider;
