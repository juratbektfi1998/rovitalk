import React from "react";
import { Link } from "react-router-dom";

import "./SlideItem.scss";

function SlideItem(props) {
  const {
    imgUrl,
    slideTitul,
    slideSubtitul,
    slideUnderSubtitul,
    slideBtnPath,
    slideBtnText,
  } = props;

  return (
    <div
      className="slide-item-header"
      style={{
        backgroundImage: `url("${imgUrl}")`,
      }}
    >
      <h1> {slideTitul} </h1>
      <h4> {slideSubtitul} </h4>
      <p> {slideUnderSubtitul} </p>
      <Link to={slideBtnPath} className="slide-btn">
        {slideBtnText}
      </Link>
    </div>
  );
}

export default SlideItem;
