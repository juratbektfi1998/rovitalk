import React from "react";
import { useTranslation } from "react-i18next";
import { Link } from "react-router-dom";
import { Col, Row } from "reactstrap";

import "./MainsectionFive.scss";

function MainSectionFive() {
  const { t, i18n } = useTranslation();
  return (
    <div className="main-section-five mt-5">
      <h1 className="w-100 text-center"> {t(`home_card_five.nameTitul`)} </h1>

      <p className="text-muted"> {t(`home_card_five.pageTitul`)} </p>

      <Row>
        <Col md="6" sm="12" className="mt-3">
          <div className="left-item">
            <div className="mb-2">
              <h3>
                {" "}
                <i class="fas fa-road"></i> {t(`home_card_five.pageTitle2`)}{" "}
              </h3>
              <p>{t(`home_card_five.pageTitle3`)}</p>
            </div>
            <div className="mb-2">
              <h3>
                {" "}
                <i class="fas fa-moon"></i> {t(`home_card_five.pageTitle4`)}
              </h3>
              <p>{t(`home_card_five.pageTitle5`)}</p>
            </div>
            <div className="mb-2">
              <h3>
                <i class="far fa-check-square"></i>{" "}
                {t(`home_card_five.pageTitle6`)}
              </h3>
              <p>{t(`home_card_five.pageTitle7`)}</p>
            </div>
            <Link to="/dashboard/psihologiya">
              {" "}
              {t(`home_card_five.pageTitle8`)}
              <i class="fas fa-info"></i>{" "}
            </Link>
          </div>
        </Col>
        <Col md="6" sm="12" className="mt-3">
          <div className="right-item">
            <Row>
              <Col md="6" sm="12">
                <img
                  src="http://rovitalk.com/wp-content/uploads/2019/10/alkimyogar.jpg"
                  alt="left-item"
                />
                <h4 className="mt-2 font-weight-bold">
                  {" "}
                  {t(`home_card_five.pageTitle9`)}{" "}
                </h4>
              </Col>
              <Col md="6" sm="12">
                <img
                  src="http://rovitalk.com/wp-content/uploads/2019/10/eng_yahshi_davo.jpg"
                  alt="right-img"
                />
                <h4 className="mt-2 font-weight-bold">
                  {" "}
                  {t(`home_card_five.pageTitle`)}{" "}
                </h4>
              </Col>
            </Row>
          </div>
        </Col>
      </Row>

      <div className="d-flex justify-content-center mt-3">
        <Link to={`/dashboard/ertakterapiya`} className="btn-link shadow-sm">
          <i class="fas fa-music"></i> {t(`home_card_five.musicTitul`)}
        </Link>
      </div>
    </div>
  );
}

export default MainSectionFive;
