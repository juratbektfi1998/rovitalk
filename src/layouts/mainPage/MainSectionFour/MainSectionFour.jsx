import React from "react";
import { useTranslation } from "react-i18next";
import { Link } from "react-router-dom";
import { Col, Row } from "reactstrap";

import "./MainSectionFour.scss";

function MainSectionFour() {
  const { t, i18n } = useTranslation();
  const imageData = [
    {
      imgPath: "http://rovitalk.com/wp-content/uploads/2019/10/m_4-1.png",
      imgAlt: "Ertaklar olami",
    },
    {
      imgPath: "http://rovitalk.com/wp-content/uploads/2019/10/m_5.png",
      imgAlt: "Ertaklar olami",
    },
    {
      imgPath: "http://rovitalk.com/wp-content/uploads/2019/10/m_2.png",
      imgAlt: "Ertaklar olami",
    },
    {
      imgPath: "http://rovitalk.com/wp-content/uploads/2019/10/m_3.png",
      imgAlt: "Ertaklar olami",
    },
    {
      imgPath: "http://rovitalk.com/wp-content/uploads/2019/10/m_1.png",
      imgAlt: "Ertaklar olami",
    },
  ];

  return (
    <div className="main-section-four mt-5">
      <h1 className="w-100 text-center ">{t(`home_card_four.nameTitul`)}</h1>
      <Row>
        <Col md="6" sm="12" className="mt-3">
          <div className="left-item">
            {imageData.map((image) => (
              <img src={image.imgPath} alt={image.imgAlt} />
            ))}
          </div>
        </Col>
        <Col md="6" sm="12" className="mt-3">
          <div className="right-item">
            <div className="right-header">
              <h4>{t(`home_card_four.Titul`)}</h4>
            </div>
            <p>{t(`home_card_four.pageTitul`)}</p>
            <Link to="/meditatsiya">
              {" "}
              {t(`home_card_four.buttonTitul`)} <i class="fas fa-info"></i>{" "}
            </Link>
          </div>
        </Col>
      </Row>
      <div className="d-flex justify-content-center mt-3">
        <Link to={`/ertaklar`} className="btn-link shadow-sm">
          <i class="fas fa-music"></i> {t(`home_card_four.nameTitul`)}
        </Link>
      </div>
    </div>
  );
}

export default MainSectionFour;
