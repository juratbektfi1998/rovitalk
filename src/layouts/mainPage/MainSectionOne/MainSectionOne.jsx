import React from "react";
import { useTranslation } from "react-i18next";
import { Col, Row } from "reactstrap";

import "./MainSectionOne.scss";
import SectionOneCard from "./SectionOneCard/SectionOneCard";

function MainSectionOne() {
  const { t, i18n } = useTranslation();
  return (
    <div className="mt-5 main-section-one">
      <Row>
        <Col md="4" sm="12">
          <div className="left-item">
            <SectionOneCard
              cardPath="/ertaklar"
              cardEmoji="far fa-smile"
              cardTitul={t(`home_card_one.cardTitul`)}
              cardSubtitul={t(`home_card_one.cardSubtitul`)}
            />
            <SectionOneCard
              cardPath="/ertaklar"
              cardEmoji="fas fa-umbrella-beach"
              cardTitul={t(`home_card_one.cardTitul2`)}
              cardSubtitul={t(`home_card_one.cardSubtitul2`)}
            />
          </div>
        </Col>
        <Col md="4" sm="12">
          <div className="mid-item">
            <img
              src="http://rovitalk.com/wp-content/uploads/2019/10/phone-x-4.png"
              alt="Phone"
              className="mid-image"
            />
          </div>
        </Col>
        <Col md="4" sm="12">
          <div className="right-item">
            <SectionOneCard
              cardPath="/ertaklar"
              cardEmoji="far fa-smile"
              cardTitul={t(`home_card_one.cardTitul3`)}
              cardSubtitul={t(`home_card_one.cardSubtitul3`)}
            />
            <SectionOneCard
              cardPath="/ertaklar"
              cardEmoji="fas fa-sun"
              cardTitul={t(`home_card_one.cardTitul4`)}
              cardSubtitul={t(`home_card_one.cardSubtitul4`)}
            />
          </div>
        </Col>
      </Row>
    </div>
  );
}

export default MainSectionOne;
