import React from "react";
import { Link } from "react-router-dom";
import "./SectionOneCard.scss";

function SectionOneCard(props) {
  const { cardPath, cardTitul, cardSubtitul, cardEmoji } = props;

  return (
    <div className="section-one-card">
      <Link to={cardPath}>
        <i className={cardEmoji}></i>
        <h3>{cardTitul}</h3>
      </Link>
      <p>{cardSubtitul}</p>
    </div>
  );
}

export default SectionOneCard;
