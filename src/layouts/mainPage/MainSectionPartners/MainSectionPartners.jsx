import React from "react";
import { useTranslation } from "react-i18next";

import "./MainSectionPartners.scss";
import PartnerSlider from "./PartnerSlider/PartnerSlider";

function MainSectionPartners() {
  const { t, i18n } = useTranslation();
  return (
    <div className="main-section-partners my-5">
      <h1 className="w-100 text-center"> {t(`home_card_Partners.Titul`)} </h1>
      <p className="w-100 text-muted text-center">
        {t(`home_card_Partners.Titul2`)}
      </p>
      <PartnerSlider />
    </div>
  );
}

export default MainSectionPartners;
