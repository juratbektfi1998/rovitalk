import React from "react";
import Slider from "react-slick";

import "./PartnerSlider.scss";
function PartnerSlider() {
  return (
    <div className="mt-2 partner-slider">
        <div className="slider-item">
          <img
            src="http://rovitalk.com/wp-content/uploads/2019/10/giza.png"
            alt="partner-one"
          />
        </div>
        <div className="slider-item">
          <img
            src="http://rovitalk.com/wp-content/uploads/2019/10/logo_12.png"
            alt="partner-one"
          />
        </div>
        <div className="slider-item">
          <img
            src="http://rovitalk.com/wp-content/uploads/2019/10/freshlife.png"
            alt="partner-one"
          />
        </div>
        <div className="slider-item">
          <img
            src="http://rovitalk.com/wp-content/uploads/2019/10/logo_13.png"
            alt="partner-one"
          />
        </div>
        <div className="slider-item">
          <img
            src="http://rovitalk.com/wp-content/uploads/2019/10/kayros.png"
            alt="partner-one"
          />
        </div>
    </div>
  );
}

export default PartnerSlider;
