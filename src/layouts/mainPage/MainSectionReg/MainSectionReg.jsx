import React from "react";
import { useTranslation } from "react-i18next";
import { Link } from "react-router-dom";
import { Button, Col, Row } from "reactstrap";
import ModalItem from "../../../components/ModalItem/ModalItem";
import Registration from "../../../views/home/Registration/Registration";

import "./MainSectionReg.scss";

function MainSectionReg() {
  const { t, i18n } = useTranslation();
  return (
    <div className="main-section-reg mt-5">
      <Row>
        <Col md="8" sm="12">
          <div className="left-box">
            <h1 className="text-white font-weight-bolder">
              {" "}
              {t(`home_card_Reg.Titul`)}!{" "}
            </h1>
            <p className="text-white font-weight-bolder">
              {t(`home_card_Reg.Titul2`)}
            </p>
          </div>
        </Col>
        <Col md="4" sm="12">
          <div className="right-box">
            
          <ModalItem
                  buttonLabel={t(`home_main_Login.pageTitle9`)}
                  title={t(`home_main_Login.pageTitle9`)}
                  component={<Registration />}
                  btnClass="reg-right-button"
                
                />
               
          </div>
        </Col>
      </Row>
    </div>
  );
}

export default MainSectionReg;
