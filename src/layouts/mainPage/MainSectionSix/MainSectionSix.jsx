import React from "react";
import { Col, Row } from "reactstrap";
import MainSectionSixSlider from "./MainSectionSixLeftSlider/MainSectionSixSlider";
import { Link } from "react-router-dom";
import "./MainSectionSix.scss";
import { useTranslation } from "react-i18next";

function MainSectionSix() {
  const { t } = useTranslation();
  return (
    <div className="main-section-six mt-5">
      <h1 className="w-100 text-center"> {t(`home_card_six.nameTitul`)} </h1>
      <Row>
        <Col md="5" sm="12" className="mt-3">
          <MainSectionSixSlider />
        </Col>
        <Col md="7" sm="12" className="mt-3">
          <div className="right-box">
            <p className="text-muted mb-3">{t(`home_card_six.pageTitul`)} </p>
            <p className="text-muted mb-3">{t(`home_card_six.pageTitul2`)} </p>
            <p className="text-muted mb-3">{t(`home_card_six.pageTitul3`)} </p>
            <Link to="/ertaklar">
              {" "}
              {t(`home_card_six.buttonTitul`)} <i className="fas fa-info"/>{" "}
            </Link>
          </div>
        </Col>
      </Row>

      <div className="d-flex justify-content-center mt-3">
        <Link to={`/ertaklar`} className="btn-link shadow-sm">
          <i className="fas fa-music"/> {t(`home_card_six.musicTitul`)}
        </Link>
      </div>
    </div>
  );
}

export default MainSectionSix;
