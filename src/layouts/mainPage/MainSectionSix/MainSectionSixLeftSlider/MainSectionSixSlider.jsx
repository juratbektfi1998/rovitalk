import React from "react";
import Slider from "react-slick";

import "./MainSectionSixSlider.scss";
function MainSectionSixSlider() {
  const settings = {
    dots: true,
    fade: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 3000,
    pauseOnHover: true,
  };

  return (
    <div className="main-section-six-slider">
      <Slider {...settings}>
        <div>
          <img
            src={`http://rovitalk.com/wp-content/uploads/2019/10/111.jpg`}
            style={{ width: "100%", height: "auto" }}
            alt="ertaklar"
          />
        </div>
        <div>
          <img
            src={`http://rovitalk.com/wp-content/uploads/2019/10/112.jpg`}
            style={{ width: "100%", height: "auto" }}
            alt="ertaklar"
          />
        </div>
        <div>
          <img
            src={`http://rovitalk.com/wp-content/uploads/2019/10/113.jpg`}
            style={{ width: "100%", height: "auto" }}
            alt="ertaklar"
          />
        </div>
        <div>
          <img
            src={`http://rovitalk.com/wp-content/uploads/2019/10/114.jpg`}
            style={{ width: "100%", height: "auto" }}
            alt="ertaklar"
          />
        </div>
      </Slider>
    </div>
  );
}

export default MainSectionSixSlider;
