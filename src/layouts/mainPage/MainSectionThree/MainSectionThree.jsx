import React from "react";
import { Col, Row } from "reactstrap";
import MainSectionThreeCard from "./MainSectionThreeCard/MainSectionThreeCard";
import "./MainSectionThree.scss";
import { Link } from "react-router-dom";
import { useTranslation } from "react-i18next";

function MainSectionThree() {
  const { t, i18n } = useTranslation();
  const cardData = [
    {
      cardImg: "http://rovitalk.com/wp-content/uploads/2019/10/mda.jpg",
      cardImgAlt: "Mening do'stim ajdar",
      cardTitul: t(`home_card_three.cardTitul1`),
      audioPath:
        "https://interactive-examples.mdn.mozilla.net/media/cc0-audio/t-rex-roar.mp3",
    },
    {
      cardImg: "http://rovitalk.com/wp-content/uploads/2019/10/mush.jpg",
      cardImgAlt: "Mening do'stim ajdar",
      cardTitul: t(`home_card_three.cardTitul2`),
      audioPath:
        "https://interactive-examples.mdn.mozilla.net/media/cc0-audio/t-rex-roar.mp3",
    },
    {
      cardImg: "http://rovitalk.com/wp-content/uploads/2019/10/assh.jpg",
      cardImgAlt: "Mening do'stim ajdar",
      cardTitul: t(`home_card_three.cardTitul3`),
      audioPath:
        "https://interactive-examples.mdn.mozilla.net/media/cc0-audio/t-rex-roar.mp3",
    },
    {
      cardImg:
        "http://rovitalk.com/wp-content/uploads/2019/10/mitti_mahluqchalar.jpg",
      cardImgAlt: "Mening do'stim ajdar",
      cardTitul: t(`home_card_three.cardTitul4`),
      audioPath:
        "https://interactive-examples.mdn.mozilla.net/media/cc0-audio/t-rex-roar.mp3",
    },
  ];

  return (
    <div className="main-section-three mt-5">
      <h4 className="text-muted text-center">{t(`home_card_three.Titul`)}</h4>

      <Row className="mt-4">
        {cardData.map((card) => (
          <Col md="6" sm="12" lg="3">
            <MainSectionThreeCard
              cardImg={card.cardImg}
              cardImgAlt={card.cardImgAlt}
              audioPath={card.audioPath}
              cardTitul={card.cardTitul}
            />
          </Col>
        ))}
      </Row>

      <div className="d-flex justify-content-center mt-3">
        <Link to={`/ertaklar`} className="btn-link shadow-sm">
          <i class="fas fa-music"></i> {t(`home_card_three.musicTitul`)}
        </Link>
      </div>
    </div>
  );
}

export default MainSectionThree;
