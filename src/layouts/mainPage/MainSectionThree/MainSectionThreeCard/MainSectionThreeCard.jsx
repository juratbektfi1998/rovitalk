import React from "react";
import ReactAudioPlayer from "react-audio-player";
import "./MainSectionThreeCard.scss";

function MainSectionThreeCard(props) {
  const { cardImg, cardImgAlt, audioPath, cardTitul } = props;

  return (
    <div className="main-section-three-card mt-2">
      <img src={cardImg} alt={cardImgAlt} />
      <ReactAudioPlayer
        src={audioPath}
        controls
        preload="metadata"
        className="audio-player"
        controlsList="nodownload"
      />
      <p className="mt-2 text-muted text-center">{cardTitul}</p>
    </div>
  );
}

export default MainSectionThreeCard;
