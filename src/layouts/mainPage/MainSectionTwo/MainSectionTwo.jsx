import React from "react";
import { useTranslation } from "react-i18next";
import { Link } from "react-router-dom";
import { Col, Row } from "reactstrap";
import "./MainSectionTwo.scss";

function MainSectionTwo() {
  const { t, i18n } = useTranslation();
  return (
    <div className="main-section-two mt-5">
      <h1>{t(`home_card_two.Titul`)}</h1>
      <Row>
        <Col md="5" sm="12" className="mt-3">
          <div className="left-box">
            <p>{t(`home_card_two.Titul2`)}</p>
            <p>{t(`home_card_two.Titul3`)}</p>
            <Link to="/ertaklar">
              {" "}
              {t(`home_card_two.Titul4`)} <i class="fas fa-info"></i>
            </Link>
          </div>
        </Col>
        <Col md="7" sm="12" className="mt-3">
          <div className="right-box">
            <img
              src="http://rovitalk.com/wp-content/uploads/2019/10/img2-1.png"
              alt="som alt data"
            />
          </div>
        </Col>
      </Row>
    </div>
  );
}

export default MainSectionTwo;
