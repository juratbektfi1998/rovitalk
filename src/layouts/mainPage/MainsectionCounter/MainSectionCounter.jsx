import React, { useState, useEffect } from "react";
import { useTranslation } from "react-i18next";
import { Col, Row } from "reactstrap";
import CounterItem from "../../../components/Counter/CounterItem";

import "./MainSectionCounter.scss";
import axios from "../../../config/axios-interceptor";

function MainSectionCounter() {
  const [data, setData] = useState([{count: 0, title: ''}, {count: 0, title: ''}, {count: 0, title: ''}, {count: 0, title: ''}]);
  const { t } = useTranslation();
  // const checkScrollTop = () => {
  //   if (window.pageYOffset > 3600) {
  //     axios.post('sections-statistics').then(res => setData(res.data.data))
  //   } else if (window.pageYOffset <= 3600) {
  //      setData([{count: 0, title: t(`home_card_Counter.Titul`)},
  //        {count: 0, title: t(`home_card_Counter.Titul2`)},
  //        {count: 0, title: t(`home_card_Counter.Titul3`)},
  //        {count: 0, title: t(`home_card_Counter.Titul4`)}])
  //   }
  // };

  useEffect(()=>{
    axios.post('sections-statistics').then(res => setData(res.data.data))
  }, []);

  // window.addEventListener("scroll", checkScrollTop);

  return (
    <div className="main-section-counter mt-5">
      <Row>{data.map(item => (
            <Col sm="12" md="6" lg="3" className="mt-3">
              <CounterItem
                  endCount={item.count}
                  countText={item.title}
              />
            </Col>
        ))}
      </Row>
    </div>
  );
}

export default MainSectionCounter;
