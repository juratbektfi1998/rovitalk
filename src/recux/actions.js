export const FETCH_NAV_ITEMS = 'navbar/FETCH_ITEMS';
export const FETCH_NAV_ITEMS_ERR = 'navbar/FETCH_ITEMS_ERR';

export const ADD_PRICE = 'prices/ADD_PRICE';
export const PRICES_RESET = 'price/PRICES_RESET';
export const GET_PROFILE = 'profile/GET_PROFILE';