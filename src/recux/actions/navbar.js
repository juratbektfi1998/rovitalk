import axios from "../../config/axios-interceptor";
import {FETCH_NAV_ITEMS, FETCH_NAV_ITEMS_ERR} from "../actions";

export const getNavItems = () => async dispatch => {
    try {
        const result = await axios.post('sections');
        dispatch({type: FETCH_NAV_ITEMS, payload: result})
    } catch (err) {
        dispatch({type: FETCH_NAV_ITEMS_ERR, payload: err})
    }
};