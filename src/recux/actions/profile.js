import {GET_PROFILE} from "../actions";
import axios from '../../config/axios-interceptor';

export const getProfile = (payload) => ({
    type: GET_PROFILE,
    payload
});

export const getUserProfile = () => async dispatch => {
    try {
        const res = await axios.get('profile/get');
        dispatch(getProfile(res.data.data))
    }  catch (e) {
        alert(e)
    }
};