import {FETCH_NAV_ITEMS, FETCH_NAV_ITEMS_ERR} from "../actions";

const initialState = {
    errorMessage: null,
    items: [],
};

export const navbarReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_NAV_ITEMS:
            return {
                ...state,
                items: action.payload.data.data
            };
        case FETCH_NAV_ITEMS_ERR:
            return {
                ...state,
                errorMessage: action.payload
            };
        default:
            return state
    }
};