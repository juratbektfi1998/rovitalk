import {GET_PROFILE} from "../actions";

const initialState = {
    errorMessage: null,
    profile: {
        first_name: "",
        last_name: "",
        phone_number: ""
    },
};

export default (state = initialState, action) => {
    switch (action.type) {
        case GET_PROFILE:
            return {
                ...state,
                profile: action.payload
            };
        default:
            return state
    }
}