import {combineReducers} from 'redux';
import {navbarReducer} from "./reducers/navbar.reducer";
import profileReducer from "./reducers/profile.reducer";

const rootReducer = combineReducers({
    navbarReducer,
    profileReducer
});

export default rootReducer;