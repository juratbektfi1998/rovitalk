import React from "react";
import { Route, Switch } from "react-router-dom";
import { PrivateRoute } from "./PrivateRoute";
import Dashboard from "./views/dashboard/Dashboard";
import Home from "./views/home/Home";

function routes() {
  return (
    <Switch>
      {/* exact page  yangi route file*/}

      <PrivateRoute path={`/dashboard`}>
        {" "}
        <Dashboard />{" "}
      </PrivateRoute>

      <Route path={`/`} component={Home} />
    </Switch>
  );
}

export default routes;
