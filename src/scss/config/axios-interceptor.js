import axios from 'axios';

import {AXIOS_TIMEOUT, SERVER_API_URL} from './constants';

export default axios.create({
    baseURL: SERVER_API_URL,
    timeout: AXIOS_TIMEOUT,
    headers: {
        role: "user"
    }
});

const onRequestSuccess = config => {
    const token = localStorage.getItem('authenticationToken');
    if (token) {
        config.headers.Authorization = `Bearer ${token}`;
    }
    return config;
};
const onResponseSuccess = response => response;
const onResponseError = err => {
    const status = err.status || err.response.status;
    if (status === 401 || status === 403) {
        localStorage.removeItem('authenticationToken');
        window.location = '/'
    }
    alert(err);
    return Promise.reject(err);
};
axios.interceptors.request.use(onRequestSuccess);
axios.interceptors.response.use(onResponseSuccess, onResponseError);

