import React, { useEffect, useState } from "react";
import { Col, Row, Table } from "reactstrap";
import TitleHamlet from "../../../components/TitleHamlet/TitleHamlet";
import axios from '../../../config/axios-interceptor';

import PricingCard from "../../home/PricingPlans/PricingCard/PricingCard";

function Audiokitoblar() {
  const [data, setData] = useState([{section_title: 'nimaidr', bought_date: 651065165, expire_date: 6516516531}]);

  useEffect(() => {
    window.scrollTo({ top: 0, behavior: "smooth" });
  }, []);

  const cardData = [
    {
      cardTitul: "Audiokitoblar",
      monthNumber: "1",
      sum: "100000",
      dol: "700",
      rubl: "10",
      korPath: "/korzonka",
      cardPath: "/cardpath",
    },
    {
      cardTitul: "Audiokitoblar",
      monthNumber: "3",
      sum: "200000",
      dol: "1400",
      rubl: "20",
      korPath: "/korzonka",
      cardPath: "/cardpath",
    },
    {
      cardTitul: "Audiokitoblar",
      monthNumber: "6",
      sum: "400000",
      dol: "2800",
      rubl: "40",
      korPath: "/korzonka",
      cardPath: "/cardpath",
    },
    {
      cardTitul: "Ertakterapiya",
      monthNumber: "1",
      sum: "50000",
      dol: "400",
      rubl: "6",
      korPath: "/korzonka",
      cardPath: "/cardpath",
    },
    {
      cardTitul: "Ertakterapiya",
      monthNumber: "3",
      sum: "100000",
      dol: "800",
      rubl: "12",
      korPath: "/korzonka",
      cardPath: "/cardpath",
    },
    {
      cardTitul: "Ertakterapiya",
      monthNumber: "6",
      sum: "200000",
      dol: "1600",
      rubl: "25",
      korPath: "/korzonka",
      cardPath: "/cardpath",
    },
    {
      cardTitul: "Hikmatlar",
      monthNumber: "1",
      sum: "100000",
      dol: "700",
      rubl: "10",
      korPath: "/korzonka",
      cardPath: "/cardpath",
    },
    {
      cardTitul: "Hikmatlar",
      monthNumber: "3",
      sum: "200000",
      dol: "1400",
      rubl: "20",
      korPath: "/korzonka",
      cardPath: "/cardpath",
    },
    {
      cardTitul: "Hikmatlar",
      monthNumber: "6",
      sum: "400000",
      dol: "2800",
      rubl: "40",
      korPath: "/korzonka",
      cardPath: "/cardpath",
    },
    {
      cardTitul: "Meditatsiya",
      monthNumber: "1",
      sum: "200000",
      dol: "1400",
      rubl: "20",
      korPath: "/korzonka",
      cardPath: "/cardpath",
    },
    {
      cardTitul: "Meditatsiya",
      monthNumber: "3",
      sum: "400000",
      dol: "2800",
      rubl: "40",
      korPath: "/korzonka",
      cardPath: "/cardpath",
    },
    {
      cardTitul: "Meditatsiya",
      monthNumber: "6",
      sum: "800000",
      dol: "5600",
      rubl: "80",
      korPath: "/korzonka",
      cardPath: "/cardpath",
    },
  ];

  useEffect(()=>{
    axios.post('active_sections').then(res => setData(res.data.data)).catch(err => alert(err))
  }, []);

  return (
    <div>
      <TitleHamlet title="Audiokitoblar" />
      <Row>
        <Col sm="12" md={{ size: 8, offset: 2 }}>
          <div>
            <Table bordered hover responsive>
              <thead>
                <tr>
                  <th>#</th>
                  <th>Activ tarif nomi</th>
                  <th>Boshlangan sana</th>
                  <th>Oxirgi sana</th>
                </tr>
              </thead>
              {data.length > 0 ? (
                  <tbody>
                  {data.map((item, key) => (
                      <tr>
                        <td>{key + 1}</td>
                        <td>{item.section_title}</td>
                        <td>{new Date(item.bought_date).toString()}</td>
                        <td>{new Date(item.expire_date).toString()}</td>
                      </tr>
                  ))}
                  </tbody>
              ) : (
                  <h4 className="text-center">No data</h4>
              )}
            </Table>
          </div>
        </Col>
      </Row>
      <div className="my-5">
        <Row>
          <Col sm="12" md={{ size: 8, offset: 2 }}>
            <Row className="flex-wrap">
              {cardData.map((card, index) => (
                <Col sm="12" md="6" lg="4" className="mb-3">
                  <PricingCard
                    key={index}
                    cardTitul={card.cardTitul}
                    monthNumber={card.monthNumber}
                    sum={card.sum}
                    dol={card.dol}
                    rubl={card.rubl}
                    korPath={card.korPath}
                    cardPath={card.cardPath}
                  />
                </Col>
              ))}
            </Row>
          </Col>
        </Row>
      </div>
    </div>
  );
}

export default Audiokitoblar;
