import React, {useState} from "react";
import { Link } from "react-router-dom";
import { Table, Button, Row, Col } from "reactstrap";
import TitleHamlet from "../../../components/TitleHamlet/TitleHamlet";

import "./CartPage.scss";
function CartPage({ tarifTitul = "Audiokitoblar 1 oylik tarif" }) {
  const [data, setData] = useState(JSON.parse(localStorage.getItem('prices')) || []);

  const deletePrice = (id) => {
    const newData = data.filter(price => price._id !== id);
    console.log(newData);
    if (newData.length > 0) {
      localStorage.setItem('prices', JSON.stringify(data));
    } else {
      localStorage.removeItem('prices');
    }
    setData(newData)
  };

  const getTotal = () => {
    const prices = JSON.parse(localStorage.getItem('prices')) || [];
    let result = 0;
    prices.map(item => result += item.cost);
    return result
  };

  return (
    <div>
      <TitleHamlet title="Tariflar" />
      <div className="flex-wrap d-flex justify-content-end align-content-center">
        <Table bordered hover responsive>
          <thead>
            <tr>
              <th colSpan="3" className="text-center">
                Mahsulotlar
              </th>
              <th>Summa</th>
            </tr>
          </thead>
          <tbody>
          {data.length > 0 ? data.map(price => (
              <tr>
                <td className="text-center">
                  <Button className="btn-danger">
                    <i className="fas fa-trash-alt" onClick={()=> deletePrice(price['_id'])}/>
                  </Button>
                </td>
                <td>
                  <img
                      src="http://rovitalk.com/wp-content/uploads/2019/11/audiokitoblar_1_oylik-300x300.jpg"
                      alt="kalendar"
                      width="50"
                      height="50"
                  />
                </td>
                <td>{price.title} {price.monthNumber} oylik tarif</td>
                <td>{price.cost}</td>
              </tr>
          )) : (
              <tr>
                <td colSpan={6} className="text-center">
                  no data
                </td>
              </tr>
          )}

          </tbody>
        </Table>
        <Button className="bg-success" onClick={()=> window.location.reload()}>Obnovit korzinku</Button>
      </div>

      <Row className="justify-content-end">
        <Col sm="12" md="6">
          <h3 className="mb-3 font-weight-bold">Сумма заказов</h3>

          <Table bordered hover responsive className="mb-3">
            {/*<tr>*/}
            {/*  <td className="font-weight-bold"> Подытог</td>*/}
            {/*  <td>100 000UZS</td>*/}
            {/*</tr>*/}
            <tr>
              <td className="font-weight-bold">Итого</td>
              <td className="font-weight-bold">{getTotal()}</td>
            </tr>
          </Table>

          <Link to="/dashboard/confirm/1">
            {" "}
            <Button className="bg-success"> Tasdiqlash </Button>
          </Link>
        </Col>
      </Row>
    </div>
  );
}

export default CartPage;
