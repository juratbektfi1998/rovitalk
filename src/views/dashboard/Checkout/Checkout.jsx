import React from "react";
import { Button } from "reactstrap";
import "./Checkout.scss";
import { Table } from "reactstrap";
import { Link } from "react-router-dom";
import TitleHamlet from "../../../components/TitleHamlet/TitleHamlet";

function Checkout({ tarifTitul = "Audiokitoblar 1 oylik tarif" }) {
  const data = JSON.parse(localStorage.getItem('prices'));

  const sendPrices = () => {
    const indexes = data.map(item => item._id);
  };

  return (
    <div>
      <TitleHamlet title="Checkout" />
      <div className="border-top flex-wrap p-3  mt-5 d-flex justify-content-between align-content-center border-success">
        <p className="p-0 mt-2">
          {" "}
          <i className=" text-success fas fa-check-circle mr-2"/> Просмотр
          корзиныВы отложили “ {tarifTitul} ” в свою корзину.{" "}
        </p>

        <Link to="/dashboard/cart/1">
          <Button className="btn-success mt-2"> Korzinka </Button>
        </Link>
      </div>
      <div className="mt-5">
        <h3 className="mb-3 font-weight-bold"> Sizning zakasingiz </h3>
        <Table bordered hover responsive>
          <thead>
            <tr>
              <th>Mahsulotlar</th>
              <th>Jami</th>
            </tr>
          </thead>
          <tbody>
          {data ? data.map(price => (
              <tr>
                <td>{price.title}</td>
                <td>{price.cost}</td>
              </tr>
          )) : (
              <tr className="text-center">
                <td colSpan={2}>Sizda tarif mavjud emas</td>
              </tr>
          )}
          </tbody>
        </Table>
        <div className="border-top flex-wrap p-3  mt-5 d-flex justify-content-between align-content-center border-success">
          <div className="d-flex align-content-center align-items-center justify-content-center">
            <div className="card px-3 font-weight-bold text-muted mr-3">
              To'lov tizimi
            </div>
            <img
              style={{ width: "150px", height: "auto" }}
              src=" http://rovitalk.com/wp-content/plugins/woocommerce-payment-gateway-master/payme.png "
              alt="payme"
            />
          </div>
          <Link to="/dashboard/confirm/1">
            <Button className="btn-success mt-2" onClick={sendPrices}> Tasdiqlash </Button>
          </Link>
        </div>
      </div>
    </div>
  );
}

export default Checkout;
