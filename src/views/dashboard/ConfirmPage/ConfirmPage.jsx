import React, {useState, useEffect} from "react";
import { Link } from "react-router-dom";
import { Col, Row, Table, Button } from "reactstrap";
import {useHistory} from 'react-router-dom';
import axios from '../../../config/axios-interceptor';
import TitleHamlet from "../../../components/TitleHamlet/TitleHamlet";
import moment from "moment/moment";

import "./ConfirmPage.scss";

export function ConfirmPage() {
  const prices = JSON.parse(localStorage.getItem('prices')).map(item => item._id) || [];
  const [payment, setPayment] = useState({number: null, date: moment().format('DD.MM.YYYY'), total: null, method: 'Payme'});
  const history = useHistory();

  const pay = () => {
    localStorage.removeItem('prices')
  };

  const goBack = () => {
    history.goBack();
  };

  useEffect(() => {
    axios.post('payme-check/create', {
      indexes: prices
    }).then(res => {
      const {data} = res.data;
      const item = {
        number: data.id,
        date: moment().format('DD.MM.YYYY'),
        total: data.amount,
        method: 'Payme',
        link: data.payme_link
      };
      setPayment(item)
    })
        .catch(err => alert(err))
  }, []);

  return (
      <div>
        <TitleHamlet title="Confirm"/>
        <Row>
          <Col sm="12" md="6">
            <Table responsive hover bordered>
              <thead>
              <tr>
                <th> НОМЕР ЗАКАЗА:</th>
                <th> ДАТА:</th>
                <th> ВСЕГО:</th>
                <th> МЕТОД ОПЛАТЫ:</th>
              </tr>
              </thead>
              <tbody>
              <tr>
                <td>{payment.number}</td>
                <td>{payment.date}</td>
                <td>{payment.total / 100}</td>
                <td>{payment.method}</td>
              </tr>
              </tbody>
            </Table>
            <p className="my-3">
              Спасибо за покупку. Нажмите "Оплатить" для продолжения.
            </p>
            <a href={payment.link || '/dashboard/confirm/1'} target="_blank" onClick={pay}>
              {" "}
              <Button className="btn-info"> To'lash </Button>
            </a>
            <Link onClick={goBack}>
              {" "}
              <Button className="btn-success">
                {" "}
                Bekor qilish va ortga qaytish{" "}
              </Button>{" "}
            </Link>
          </Col>
        </Row>
      </div>
  );
}
