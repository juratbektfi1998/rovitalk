import React, {useEffect, useState} from "react";
import {Route, Switch} from "react-router-dom";
import {Col, Container, Row} from "reactstrap";
import DashNavbar from "../../layouts/dashboard/DashNavbar/DashNavbar";
import DashSideBar from "../../layouts/dashboard/DashSideBar/DashSideBar";
import Audiokitoblar from "./Audiokitoblar/Audiokitoblar";
import CartPage from "./CartPage/CartPage";
import Checkout from "./Checkout/Checkout";
import {ConfirmPage} from "./ConfirmPage/ConfirmPage";

import "./Dashboard.scss";
import EditProfil from "./EditProfil/EditProfil";
import Ertakterapiya from "./Ertakterapiya/Ertakterapiya";
import Hikmatlar from "./Hikmatlar/Hikmatlar";
import Kuzgu from "./Kuzgu/Kuzgu";
import Psihologiya from "./Psihologiya/Psihologiya";
import Registration from "./Registration/Registration";
import SingleBookPage from "./SingleBookPage/SingleBookPage";
import SinglePricePage from "./SinglePricePage/SinglePricePage";
import TarifOlish from "./TarifOlish/TarifOlish";
import UserProfil from "./UserProfil/UserProfil";
import {useDispatch, useSelector} from "react-redux";
import {getUserProfile} from "../../recux/actions/profile";

function Dashboard() {
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(getUserProfile())
    }, []);

    const user = useSelector(store => store.profileReducer.profile);
    return (
        <div className="dashboard-page">
            <DashNavbar userName={`${user.first_name} ${user.last_name}`}/>
            <main className="dash-main">
                <div className="left-sidebar">
                    {" "}
                    <DashSideBar/>{" "}
                </div>
                <div className="right-main-page">
                    <Container fluid={true}>
                        <Row>
                            <Col sm="12" md={{size: 10, offset: 1}}>
                                <Switch>
                                    <Route
                                        path="/dashboard/ertakterapiya"
                                        component={Ertakterapiya}
                                    />
                                    <Route
                                        path="/dashboard/audiokitoblar"
                                        component={Audiokitoblar}
                                    />
                                    <Route
                                        path="/dashboard/psihologiya"
                                        component={Psihologiya}
                                    />
                                    <Route path="/dashboard/kuzgu" component={Kuzgu}/>
                                    <Route path="/dashboard/userprofil" component={UserProfil}/>
                                    <Route
                                        path="/dashboard/editprofil"
                                        exact
                                        component={EditProfil}
                                    />
                                    <Route path="/dashboard/tarifolish" component={TarifOlish}/>
                                    <Route path="/dashboard/section/:id?" component={Ertakterapiya}/>
                                    <Route
                                        path="/dashboard/registration"
                                        component={Registration}
                                    />
                                    <Route
                                        path="/dashboard/file/:id"
                                        component={SingleBookPage}
                                    />
                                    <Route
                                        path="/dashboard/singleprice/:id"
                                        component={SinglePricePage}
                                    />
                                    <Route path="/dashboard/checkout/:id" component={Checkout}/>

                                    <Route path="/dashboard/cart/:id" component={CartPage}/>
                                    <Route
                                        path="/dashboard/confirm/:id"
                                        component={ConfirmPage}
                                    />

                                    {/* this is page not found */}
                                    <Route
                                        path="/dashboard/*"
                                        render={() => <div>page not found</div>}
                                    />
                                </Switch>
                            </Col>
                        </Row>
                    </Container>
                </div>
            </main>
        </div>
    );
}

export default Dashboard;
