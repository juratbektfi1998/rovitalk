import React, {useEffect, useState} from "react";
import {Formik} from "formik";
import {useHistory} from "react-router-dom";
import "./EditProfil.scss";
import {Button, Col, CustomInput, Form, FormGroup, Input, Label, Row,} from "reactstrap";
import TitleHamlet from "../../../components/TitleHamlet/TitleHamlet";
import Axios from "../../../config/axios-interceptor";
import { useTranslation } from "react-i18next";
import FormData from 'form-data'
import {useDispatch, useSelector} from "react-redux";
import {getUserProfile} from "../../../recux/actions/profile";


const InitialValues = {
    last_name: "",
    first_name: "",
    phone_number: "",
    image: ""
};

function EditProfil() {

    const { t } = useTranslation();
    const history = useHistory();
    const [imageUrl, setImageUrl] = useState();
    const [uploading, setUploading] = useState(false);
    const dispatch = useDispatch();
    let user = useSelector(store => store.profileReducer.profile);

    const handle = (file) => {
        setUploading(true);
        // Create an object of formData
        const formData = new FormData();

        // Update the formData object
        formData.append(
            "image",
            file,
            file.name
        );
        Axios.post('upload_image', formData)
            .then(success => {
                setImageUrl(success.data.data.path);
                setUploading(false)
            })
            .catch(err => {
                alert(err);
                setUploading(false)
            })
    };

    useEffect(() => {
        window.scrollTo({top: 0, behavior: "smooth"});
    }, []);

    return (
        <div className="edit-profil-page">
            <TitleHamlet title="Edit Profil"/>
            <Row>
                <Col sm="12" md={{size: 6, offset: 3}}>
                    {" "}
                    <h1 className="my-5 w-100 text-center"> {t(`home_Edit_Profil.Titul`)} </h1>
                    <Formik
                        initialValues={InitialValues}
                        onSubmit={(values, {setSubmitting}) => {
                                if (!uploading) {
                                    setSubmitting(false);
                                    Object.keys(values).map(key => {
                                        if (values[key] && values[key] !== "") {
                                            user[key] = values[key]
                                        }
                                    });
                                    user = {...user, image: imageUrl};
                                    Axios.post('update-profile', user)
                                        .then(success => {
                                            dispatch(getUserProfile());
                                            history.push('/dashboard/ertakterapiya')
                                        })
                                        .catch(err => alert(err))
                                }
                        }}
                    >
                        {({
                              values,
                              errors,
                              touched,
                              handleChange,
                              handleBlur,
                              handleSubmit,
                              isSubmitting,
                              setFieldValue,

                              /* and other goodies */
                          }) => (
                            <Form onSubmit={handleSubmit}>
                                <FormGroup>
                                    <Label for="first_name" className="text-muted">
                                        {" "}
                                        * {t(`home_Edit_Profil.Titul2`)}{" "}
                                    </Label>
                                    <Input
                                        type="text"
                                        name="first_name"
                                        id="first_name"
                                        defaultValue={user.first_name}
                                        placeholder={t(`home_Edit_Profil.Titul3`)}
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                    />
                                    {errors.first_name && touched.first_name && errors.first_name}
                                </FormGroup>
                                <FormGroup>
                                    <Label for="last_name" className="text-muted">
                                        {" "}
                                        * {t(`home_Edit_Profil.Titul4`)}{" "}
                                    </Label>
                                    <Input
                                        type="text"
                                        name="last_name"
                                        id="last_name"
                                        defaultValue={user.last_name}
                                        placeholder={t(`home_Edit_Profil.Titul5`)}
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                    />
                                    {errors.last_name && touched.last_name && errors.last_name}
                                </FormGroup>
                                <FormGroup>
                                    <Label for="phone_number" className="text-muted">
                                        {" "}
                                        * {t(`home_Edit_Profil.Titul6`)}
                                    </Label>
                                    <Input
                                        type="text"
                                        name="phone_number"
                                        id="phone_number"
                                        defaultValue={user.phone_number}
                                        placeholder={t(`home_Edit_Profil.Titul7`)}
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                    />
                                    {errors.phone_number && touched.phone_number && errors.phone_number}
                                </FormGroup>
                                <FormGroup>
                                <Label for="image" className="text-muted" >{t(`home_Edit_Profil.Titul8`)}</Label>
                                <CustomInput   type="file"
                                        name="image"
                                        id="image"
                                        label={t(`home_Edit_Profil.Titul8`)}
                                        onChange={(event) => {
                                            handle(event.currentTarget.files[0]);
                                        }}
                                        onBlur={handleBlur}

                                        value={values.image} />
                                    {errors.image && touched.image && errors.image}
                                </FormGroup>

                                <Button
                                    type="submit"
                                    className="my-3 px-5 btn-success"
                                    disabled={isSubmitting || uploading}
                                >
                                    {t(`home_Edit_Profil.Titul9`)}
                                </Button>
                            </Form>
                        )}
                    </Formik>
                </Col>
            </Row>
        </div>
    );
}

export default EditProfil;
