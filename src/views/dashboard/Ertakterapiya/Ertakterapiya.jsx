import React, { useEffect } from "react";
import TitleHamlet from "../../../components/TitleHamlet/TitleHamlet";
import ErtakterapiyaSecOne from "../../../layouts/dashboard/Ertakterapiya/ErtakterapiyaSecOne/ErtakterapiyaSecOne";
import ErtakterapiyaSecTwo from "../../../layouts/dashboard/Ertakterapiya/ErtakterapiyaSecTwo/ErtakterapiyaSecTwo";
import MainPageHeaderSlider from "../../../layouts/mainPage/MainPageHeaderSlider/MainPageHeaderSlider";

import "./Ertakterapiya.scss";

function Ertakterapiya() {
  useEffect(() => {
    window.scrollTo({ top: 0, behavior: "smooth" });
  }, []);

  return (
    <div className="ertakterapiya-page">
      <TitleHamlet title={"ertakterapiya"} />
      <div className="mb-5 ertakterapiya-header">
        <MainPageHeaderSlider />
      </div>
      <ErtakterapiyaSecOne />
      <ErtakterapiyaSecTwo />
    </div>
  );
}

export default Ertakterapiya;
