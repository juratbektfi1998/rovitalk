import React, { useEffect } from "react";
import TitleHamlet from "../../../components/TitleHamlet/TitleHamlet";
import ErtakterapiyaSecOne from "../../../layouts/dashboard/Ertakterapiya/ErtakterapiyaSecOne/ErtakterapiyaSecOne";

function Hikmatlar() {
  useEffect(() => {
    window.scrollTo({ top: 0, behavior: "smooth" });
  }, []);

  return (
    <div className="my-5">
      <TitleHamlet title="Hikmatlar" />

      <ErtakterapiyaSecOne />
    </div>
  );
}

export default Hikmatlar;
