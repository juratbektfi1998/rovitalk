import React from "react";
import { Col, Row } from "reactstrap";
import { Formik } from "formik";
import { Button, Form, FormGroup, Label, Input } from "reactstrap";
import ModalItem from "../../../components/ModalItem/ModalItem";
import Shartlar from "../Shartlar/Shartlar";
import TitleHamlet from "../../../components/TitleHamlet/TitleHamlet";

const initialRegistrationValues = {
  login: "",
  firstName: "",
  lastName: "",
  email: "",
  parol: "",
  confirm: "",
};

function Registration() {
  return (
    <div>
      <TitleHamlet title="Registration" />

      <h3 className="my-5 w-100 text-center">Ro`yxatga olish</h3>
      <Row className="justify-content-center">
        <Col sm="12" md="6">
          <Formik
            initialValues={initialRegistrationValues}
            validate={(values) => {
              const errors = {};
              if (!values.email) {
                errors.email = "Required";
              } else if (
                !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
              ) {
                errors.email = "Invalid email address";
              }
              return errors;
            }}
            onSubmit={(values, { setSubmitting }) => {
              setTimeout(() => {
                alert(JSON.stringify(values, null, 2));
                setSubmitting(false);
              }, 400);
            }}
          >
            {({
              values,
              errors,
              touched,
              handleChange,
              handleBlur,
              handleSubmit,
              isSubmitting,
              /* and other goodies */
            }) => (
              <Form onSubmit={handleSubmit} className="mb-5">
                <FormGroup>
                  <Label for="login" className="text-muted">
                    {" "}
                    *Forydalanuvchi nomi (Login){" "}
                  </Label>
                  <Input
                    type="text"
                    name="login"
                    id="login"
                    placeholder="login"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.login}
                  />
                </FormGroup>
                {/* {errors.email && touched.email && errors.email} */}

                <FormGroup>
                  <Label for="firstName" className="text-muted">
                    {" "}
                    * Ism{" "}
                  </Label>
                  <Input
                    type="text"
                    name="firstName"
                    id="firstName"
                    placeholder="firstName"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.firstName}
                  />
                </FormGroup>
                {/* {errors.email && touched.email && errors.email} */}

                <FormGroup>
                  <Label for="lastName" className="text-muted">
                    {" "}
                    * Familiya{" "}
                  </Label>
                  <Input
                    type="text"
                    name="lastName"
                    id="lastName"
                    placeholder="lastName"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.lastName}
                  />
                </FormGroup>
                {/* {errors.email && touched.email && errors.email} */}

                <FormGroup>
                  <Label for="email" className="text-muted">
                    {" "}
                    * Email
                  </Label>
                  <Input
                    type="email"
                    name="email"
                    id="email"
                    placeholder="email"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.email}
                  />
                </FormGroup>
                {/* {errors.email && touched.email && errors.email} */}

                <FormGroup>
                  <Label for="parol" className="text-muted">
                    {" "}
                    * Parol
                  </Label>
                  <Input
                    type="password"
                    name="parol"
                    id="parol"
                    placeholder="parol"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.parol}
                  />
                </FormGroup>
                {/* {errors.email && touched.email && errors.email} */}

                <FormGroup check>
                  <Label check className="text-muted d-flex">
                    <Input
                      name="confirm"
                      type="checkbox"
                      onChange={handleChange}
                      onBlur={handleBlur}
                      value={values.confirm}
                    />{" "}
                    Rovitalkdan sotib olish va ishlash
                    <span className="mx-2 text-success">
                      <ModalItem
                        buttonLabel="shartlariga"
                        title="Shartlar"
                        component={<Shartlar />}
                      />
                    </span>
                    roziman
                  </Label>
                </FormGroup>

                <Button
                  type="submit"
                  className="btn-success my-3 px-5"
                  disabled={isSubmitting}
                >
                  Ro'yxatdan o'tish
                </Button>
              </Form>
            )}
          </Formik>
        </Col>
      </Row>
    </div>
  );
}

export default Registration;
