import React, { useEffect, useState } from "react";

import "./BgMusic.scss";

function BgMusic({
  bgaudiofile = "https://html.com/wp-content/uploads/flamingos.mp3",
  bgaudioimage = "https://www.meissl.com/media/images/8f24db1f/schweiz.jpg",
  indexfon = 0

}) {
  const useAudio = (url) => {
    const [audio] = useState(new Audio(url));
    const [playing, setPlaying] = useState(false);

    const toggle = () => {
      // playMusicHandle();
      setPlaying(!playing);
    }

    useEffect(() => {
      playing ? audio.play() : audio.pause();
    }, [playing]);

    useEffect(() => {
      audio.addEventListener("ended", () => setPlaying(false));
      return () => {
        audio.removeEventListener("ended", () => setPlaying(false));
      };
    }, []);

    return [playing, toggle];
  };
  const [playing, toggle] = useAudio(bgaudiofile);
  return (
    <div
      style={{
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignContent: "center",
        alignItems: "center",
      }}
    >
      <small> fon {indexfon} </small>
      <button
        className="bg-music-btn"
        style={{ backgroundImage: `url(${bgaudioimage})` }}
        onClick={toggle}
      >
        {playing ? (
          <div>
            {" "}
            <i class="fas fa-pause"></i>
          </div>
        ) : (
          <div>
            {" "}
            <i class="fas fa-play"></i>
          </div>
        )}
      </button>
    </div>
  );
}

export default BgMusic;
