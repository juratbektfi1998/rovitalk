import React, {useEffect, useState} from "react";
import ReactAudioPlayer from "react-audio-player";

import {Col, Row} from "reactstrap";
import TitleHamlet from "../../../components/TitleHamlet/TitleHamlet";
// import Slider from "react-slick";
import "./SingleBookPage.scss";
import axios from "../../../config/axios-interceptor";

function SingleBookPage(props) {
  const [autoplay, setAutoplay] = useState(false);
  const [playlist, setPlaylist] = useState();
  const [data, setData] = useState([]);
  const [indexIcon, setIndexIcon] = useState("");
  const [item, setItem] = useState(null);

  useEffect(() => {
    const { id } = props.match.params;
    if (id) {
      axios
        .post(`files/${id}`)
        .then((res) => {
          setData(res.data.data);
          setItem(res.data.data[0]);
        })
        .catch((err) => alert(err));
    }
  }, []);

  const handleCLick = (audioItem) => {
    setAutoplay(true);
    setItem(audioItem);
    axios
      .post(`file/${audioItem._id}`, { file_id: audioItem._id })
      .then((res) => setPlaylist(res.data.data));
  };
  const [bgmusicstyle, setbgmusicstyle] = useState(false);

  useEffect(() => {
    setTimeout(() => {
      setbgmusicstyle(true);
    }, 1000);
  }, []);

  return (
    <div className="single-book-page">
      {/* <div
        className="bg-music"
        style={{ right: bgmusicstyle ? "10px" : "-100%", maxHeight:'200px', overflowY:'auto',   }}
      >
      <BgMusic />
      <BgMusic />
      <BgMusic />
      <BgMusic />
      <BgMusic />
      <BgMusic />
    

      </div> */}
      <TitleHamlet title="Book" />
      {item ? (
        <Row>
          <Col sm="12" md="3">
            <div>
              <img src={item.image} alt="single-book page" />

              <h4 className="text-uppercase font-weight-bold my-2">
                {item.category_title}
              </h4>
            </div>
          </Col>
          <Col sm="12" md="9">
            <div className="w-100">
              <h4 className="text-muted"> {item.title} </h4>
              <ReactAudioPlayer
                src={playlist && playlist.link}
                autoPlay={autoplay}
                controls
                onPause={() => {
                  setIndexIcon("");
                }}
                preload="metadata"
                className="w-100 audio-player"
                controlsList="nodownload"
              />

              <div className="card my-2 shadow-lg rounded p-3">
                {data.map((audioItem, index) => (
                  <div key={index} onClick={() => handleCLick(audioItem)}>
                    {indexIcon === index ? (
                      <i className={`far fa-pause-circle`} />
                    ) : (
                      <i className={`far fa-play-circle`} />
                    )}{" "}
                    {audioItem.title}
                  </div>
                ))}
              </div>
            </div>
          </Col>
        </Row>
      ) : (
        <Row>
          <Col sm="12" md="3">
            <div>
              <img src="" alt="single-book page" />

              <h4 className="text-uppercase font-weight-bold my-2"/>
            </div>
          </Col>
          <Col sm="12" md="9">
            <div className="w-100">
              <h4 className="text-muted">Music</h4>
              <ReactAudioPlayer
                src={playlist}
                autoPlay={autoplay}
                controls
                onPause={() => {
                  setIndexIcon("");
                }}
                preload="metadata"
                className="w-100 audio-player"
                controlsList="nodownload"
              />

              <div className="card my-2 shadow-lg rounded p-3">
                {data.map((audioItem, index) => (
                  <div
                    key={index}
                    onClick={() => {
                      handleCLick(audioItem._id, audioItem.title, index);
                    }}
                  >
                    {indexIcon === index ? (
                      <i className={`far fa-pause-circle`}/>
                    ) : (
                      <i className={`far fa-play-circle`}/>
                    )}{" "}
                    {audioItem.audioTitle}
                  </div>
                ))}
              </div>
            </div>
          </Col>
        </Row>
      )}
    </div>
  );
}

export default SingleBookPage;
