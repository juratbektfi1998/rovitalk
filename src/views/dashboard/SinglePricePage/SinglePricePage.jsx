import React from "react";
import { Col, Row } from "reactstrap";
import TitleHamlet from "../../../components/TitleHamlet/TitleHamlet";

import PricingCard from "../../home/PricingPlans/PricingCard/PricingCard";

import "./SinglePricePage.scss";

function SinglePricePage() {
  const cardData = [
    {
      cardTitul: "Audiokitoblar",
      monthNumber: "1",
      sum: "100000",
      dol: "700",
      rubl: "10",
      korPath: "/korzonka",
      cardPath: "/dashboard/singleprice/1",
    },
  ];

  return (
    <div>
      <TitleHamlet title="Tariflar" />
      <h3 className="font-weight-bold my-3"> Audiokitoblar 1 oylik tarif </h3>

      <Row>
        <Col sm="12" md="3">
          <PricingCard
            cardTitul={cardData[0].cardTitul}
            monthNumber={cardData[0].monthNumber}
            sum={cardData[0].sum}
            dol={cardData[0].dol}
            rubl={cardData[0].rubl}
            korPath={cardData[0].korPath}
            cardPath={cardData[0].cardPath}
          />
        </Col>
        <Col sm="12" md="9">
          <h4 className="text-success"> 100 000 UZS </h4>

          <p>
            {" "}
            <b>“Audiokitoblar 1 oylik”</b> tarif rejasi.{" "}
            <em>Narhi 1 oyga 100 000 so’m.</em> Hozirda bu bo’limda faqatgina 10
            ta audio kitoblar bor. Yaqin oylar mobaynida bu bo’limga yangi
            kitoblar qo’shiladi.
          </p>
        </Col>
      </Row>
    </div>
  );
}

export default SinglePricePage;
