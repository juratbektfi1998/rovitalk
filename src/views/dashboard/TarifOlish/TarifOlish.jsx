import React, {useEffect, useState, Fragment } from "react";
import {Col, Row, Table} from "reactstrap";
import TitleHamlet from "../../../components/TitleHamlet/TitleHamlet";
import PricingCard from "../../home/PricingPlans/PricingCard/PricingCard";
import axios from "../../../config/axios-interceptor";
import "./TarifOlish.scss";


function TarifOlish() {

    const [data, setData] = useState([]);
    const [activeSections, setActiveSections] = useState([]);

    useEffect(()=>{
        window.scrollTo({top: 0, behavior: "smooth"});
    }, []);

    useEffect(() => {
        axios.post("prices").then((value => setData(value.data.data))).catch(err => alert(err))
        axios.post('active_sections')
            .then(res => setActiveSections(res.data.data))
            .catch(err => alert(err))
    }, []);

    const cardData = [];

    const convert = () => {
        data.map(item => {
            item.prices.map(price => {
                const newItem = {
                    price,
                    cardTitul: item.title,
                    monthNumber: price.duration,
                    sum: price.cost,
                    dol: "5600",
                    rubl: "80",
                    korPath: "/korzonka",
                    cardPath: "/cardpath",
                };
                cardData.push(newItem)
            })
        })
    };

    convert();

    return (
        <div>
            <TitleHamlet title="Tarif Olish"/>

            <Row>
                <Col sm="12" md={{size: 8, offset: 2}}>
                    <div>
                        <Table bordered hover responsive>
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Activ tarif nomi</th>
                                <th>Boshlangan sana</th>
                                <th>Oxirgi sana</th>
                            </tr>
                            </thead>
                            <tbody>
                            {activeSections.length > 0 ? activeSections.map((item, key) => {
                                const beginDate = new Date(parseInt(item.bought_date, 10));
                                const expireDate = new Date(parseInt(item.expire_date, 10));
                                return (
                                    <tr key={key}>
                                        <td>{key + 1}</td>
                                        <td>{item.section_title}</td>
                                        <td>{beginDate.toLocaleDateString()}</td>
                                        <td>{expireDate.toLocaleDateString()}</td>
                                    </tr>
                                )
                            }) : (
                                <tr>
                                    <td colSpan="4" className="text-center w-10 text-muted">
                                        {" "}
                                        Sizda tarif rejasi mavjud emas.{" "}
                                    </td>
                                </tr>
                            )}
                            </tbody>
                        </Table>
                    </div>
                </Col>
            </Row>
            <div className="my-5">
                <Row>
                    <Col sm="12" md={{size: 8, offset: 2}}>
                        <Row className="flex-wrap">
                            {cardData.length > 0 && cardData.map((card, index) => (
                                <Col sm="12" md="6" lg="4" className="mb-3" key={index}>
                                    <PricingCard
                                        {...card}
                                        key={index}
                                        cardTitul={card.cardTitul}
                                        monthNumber={card.monthNumber}
                                        sum={card.sum}
                                        dol={card.dol}
                                        rubl={card.rubl}
                                        korPath={card.korPath}
                                        cardPath={card.cardPath}
                                    />
                                </Col>
                            ))}
                        </Row>
                    </Col>
                </Row>
            </div>
        </div>
    );
}

export default TarifOlish;
