import React, {useEffect, useState} from "react";
import { Link } from "react-router-dom";
import {Col, Row, Table} from "reactstrap";
import TitleHamlet from "../../../components/TitleHamlet/TitleHamlet";
import axios from '../../../config/axios-interceptor';

import "./UserProfil.scss";

function UserProfil({
                        avatarImg = "http://0.gravatar.com/avatar/f3219e9f8151bed2e215c0f9b9c5a4f1?s=96&d=mm&r=g",
                        favorites = 0, watchlist = 0, ratings = 0
                    }) {

    const [data, setData] = useState([]);
    const [user, setUser] = useState({first_name: '', last_name: '', image: "http://0.gravatar.com/avatar/f3219e9f8151bed2e215c0f9b9c5a4f1?s=96&d=mm&r=g"});

    useEffect(()=>{
        window.scrollTo({top: 0, behavior: "smooth"});

    }, []);

    useEffect(() => {
        axios.post('active_sections')
            .then(res => setData(res.data.data))
            .catch(err => alert(err));

        axios.get('profile/get')
            .then(res => setUser(res.data.data))
            .catch(err => alert(err))

    }, []);

    return (
        <div className="user-profil-page">
            <TitleHamlet title="User Profil"/>{" "}
            <Row>
                <Col sm="12" md="3" className="mb-3">
                    <div className="shadow-lg" style={{position:"relative"}}>
                        
                        <Link to="/dashboard/editprofil" className="go-to-edit-icon" > <i className="fas fa-user-edit"></i> </Link>
 

                        <img src={user.image} alt="user-profil"/>
                        <h3 className="my-3 w-100 text-center">{user.first_name && user.last_name ? user.first_name + ' ' + user.last_name : ''}</h3>

                        <div className="p-3">
                            <h5 className="text-muted text-uppercase">User Stats</h5>

                            <div className="d-flex mb-3">
                                <div className="favorites w-100 border py-3">
                                    <h3 className="w-100 text-center text-success p-0 m-0">
                                        {" "}
                                        {favorites}{" "}
                                    </h3>
                                    <p className="w-100 text-center text-muted p-0 m-0">
                                        <small  className="font-weight-bolder">Favorites</small>
                                    </p>
                                </div>
                                <div className="favorites w-100 border py-3">
                                    <h3 className="w-100 text-center text-success  p-0 m-0">
                                        {" "}
                                        {watchlist}{" "}
                                    </h3>
                                    <p className="w-100 text-center text-muted  p-0 m-0">
                                        <small  className="font-weight-bolder">Watchlists</small>
                                    </p>
                                </div>
                                <div className="favorites w-100 border py-3">
                                    <h3 className="w-100 text-center text-success  p-0 m-0">
                                        {" "}
                                        {favorites}{" "}
                                    </h3>
                                    <p className="w-100 text-center text-muted p-0 m-0 ">
                                        <small className="font-weight-bolder">Favorites</small>
                                    </p>
                                </div>
                            </div>

                            <h5 className="text-muted text-uppercase">Member Since</h5>
                            <p className="text-muted">Sep 12, 2020</p>
                        </div>
                    </div>
                </Col>
                <Col sm="12" md="9" className="mb-3">
                    <div>
                        {" "}
                        <Table bordered hover responsive>
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Activ tarif nomi</th>
                                <th>Boshlangan sana</th>
                                <th>Oxirgi sana</th>
                            </tr>
                            </thead>
                            <tbody>
                            {data.length > 0 ? data.map((item, key) => {
                                const boughtDate = new Date(parseInt(item.bought_date, 10));
                                const expireDate = new Date(parseInt(item.expire_date, 10));
                                return (
                                    <tr key={key}>
                                        <td>{key + 1}</td>
                                        <td>{item.section_title}</td>
                                        <td>{boughtDate.toLocaleDateString()}</td>
                                        <td>{expireDate.toLocaleDateString()}</td>
                                    </tr>
                                )
                            }) : (
                                <tr>
                                    <td colSpan="4" className="text-center w-10 text-muted">
                                        {" "}
                                        Sizda tarif rejasi mavjud emas.{" "}
                                    </td>
                                </tr>
                            )}
                            </tbody>
                        </Table>
                    </div>
                </Col>
            </Row>{" "}
        </div>
    );
}

export default UserProfil;
