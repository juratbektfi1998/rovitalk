import React, { useEffect } from "react";
import { useTranslation } from "react-i18next";
import { Col, Row } from "reactstrap";

function Ertaklar() {
  useEffect(() => {
    window.scrollTo({ top: 0, behavior: "smooth" });
  }, []);
  const { t, i18n } = useTranslation();
  return (
    <div>
      <Row>
        <Col className="text-center font-weight-bold">
          <h1>{t(`home_card_Ertak.head_Titul`)}</h1>
        </Col>
      </Row>
      <div className="ertakterapiya-sec-two mb-5 mt-5">
        <div className="section-one mb-3">
          <h3 className="font-weight-bold w-100 text-center mb-3">
            {" "}
            {t(`home_card_Ertak.body_Titul`)}?{" "}
          </h3>
          <Row>
            <Col sm="12" md="8">
              <div className="left-box">
                <p className="text-muted">{t(`home_card_Ertak.page_Titul`)}</p>
                <p className="text-muted">{t(`home_card_Ertak.page_Titul2`)}</p>
              </div>
            </Col>
            <Col sm="12" md="4">
              <div className="right-box">
                <img
                  src="http://rovitalk.com/wp-content/uploads/2019/10/img2.png"
                  alt="right-img"
                />
              </div>
            </Col>
          </Row>
        </div>

        <div className="section-two mb-3">
          <h3 className="font-weight-bold w-100 text-center mb-3">
            {" "}
            {t(`home_card_Ertak.body_Titul2`)}?{" "}
          </h3>
          <p className="text-muted">{t(`home_card_Ertak.page_Titul3`)}</p>
          <p className="text-muted">{t(`home_card_Ertak.page_Title4`)}</p>
          <p className="text-muted">{t(`home_card_Ertak.page_Title5`)}</p>
        </div>

        <div className="section-three">
          <h3 className="font-weight-bold w-100 text-center mb-3">
            {" "}
            {t(`home_card_Ertak.body_Titul3`)}{" "}
          </h3>
          <p className="text-muted">{t(`home_card_Ertak.page_Title6`)}</p>
        </div>

        <div className="section-four">
          <h3 className="font-weight-bold w-100 text-center mb-3">
            {" "}
            {t(`home_card_Ertak.body_Titul4`)}{" "}
          </h3>

          <p className="text-muted">{t(`home_card_Ertak.page_Title7`)}</p>

          <Row>
            <Col sm="12" md="4">
              <div className="mb-3">
                <img
                  src="http://rovitalk.com/wp-content/uploads/2019/10/1_111_1.png"
                  alt="left-img"
                />
              </div>
            </Col>
            <Col sm="12" md="8">
              <div className="mb-3">
                <p className="text-muted mb-2">
                  {t(`home_card_Ertak.page_Title8`)}
                </p>
                <ul>
                  <li>
                    <p className="text-muted">
                      {t(`home_card_Ertak.page_Title9`)}
                    </p>
                  </li>
                  <li>
                    {" "}
                    <p className="text-muted">
                      {t(`home_card_Ertak.page_Title10`)}
                    </p>{" "}
                  </li>
                  <li>
                    <p className="text-muted">
                      {" "}
                      {t(`home_card_Ertak.page_Title11`)}{" "}
                    </p>
                  </li>
                </ul>

                <p className="text-muted">
                  {" "}
                  {t(`home_card_Ertak.page_Title12`)}{" "}
                </p>
              </div>
            </Col>
          </Row>
        </div>

        <h3 className="font-weight-bold w-100 text-center mb-3">
          {" "}
          {t(`home_card_Ertak.page_Title13`)}{" "}
        </h3>

        <p className="text-muted">{t(`home_card_Ertak.page_Title14`)}</p>
        <br />
        <p className="text-muted">{t(`home_card_Ertak.page_Title15`)}</p>

        <Row>
          <Col sm="12" md="4">
            <div>
              <img
                src="http://rovitalk.com/wp-content/uploads/2019/10/photo_11.png"
                alt="one"
              />
              <h3 className="w-100 text-center text-uppercase text-success font-weight-bold my-2">
                {" "}
                {t(`home_card_Ertak.page_Title16`)}{" "}
              </h3>
              <p className="text-center w-100 text-muted">
                {t(`home_card_Ertak.page_Title17`)}
              </p>
            </div>
          </Col>
          <Col sm="12" md="4">
            <div>
              <img
                src="http://rovitalk.com/wp-content/uploads/2019/10/photo_21.png"
                alt="one"
              />
              <h3 className="w-100 text-center text-uppercase text-success font-weight-bold my-2">
                {" "}
                {t(`home_card_Ertak.page_Title18`)}{" "}
              </h3>
              <p className="text-center w-100 text-muted">
                {t(`home_card_Ertak.page_Title19`)}
              </p>
            </div>
          </Col>
          <Col sm="12" md="4">
            <div>
              <img
                src="http://rovitalk.com/wp-content/uploads/2019/10/photo_31.png"
                alt="one"
              />
              <h3 className="w-100 text-center text-uppercase text-success font-weight-bold my-2">
                {" "}
                {t(`home_card_Ertak.page_Title20`)}{" "}
              </h3>
              <p className="text-center w-100 text-muted">
                {t(`home_card_Ertak.page_Title21`)}
              </p>
            </div>
          </Col>
        </Row>

        <h3 className="font-weight-bold w-100 text-center mb-3">
          {" "}
          {t(`home_card_Ertak.page_Title22`)}{" "}
        </h3>

        <p className="text-muted">{t(`home_card_Ertak.page_Title23`)}</p>
        <br />
        <p className="text-muted">{t(`home_card_Ertak.page_Title24`)}</p>
        <br />
        <p className="text-muted">{t(`home_card_Ertak.page_Title25`)}</p>
        <br />
        <p className="text-muted">{t(`home_card_Ertak.page_Title26`)}</p>

        <h3 className="font-weight-bold w-100 text-center mb-3">
          {" "}
          {t(`home_card_Ertak.page_Title27`)}{" "}
        </h3>
        <p className="text-muted">{t(`home_card_Ertak.page_Title28`)}</p>
        <Row>
          <Col sm="12" md="3">
            <div>
              <img
                src="http://rovitalk.com/wp-content/uploads/2019/10/2222.png"
                alt="left-data"
              />
            </div>
          </Col>
          <Col sm="12" className="pt-5" md="9">
            <div>
              <p className="text-muted"> {t(`home_card_Ertak.page_Title29`)}</p>
              <p className="text-muted">{t(`home_card_Ertak.page_Title30`)}</p>
              <p className="text-muted">{t(`home_card_Ertak.page_Title31`)}</p>
              <p className="text-muted">{t(`home_card_Ertak.page_Title32`)} </p>
              <p className="text-muted">
                {t(`home_card_Ertak.page_Title33`)}
              </p>{" "}
              <p className="text-muted">{t(`home_card_Ertak.page_Title34`)}</p>{" "}
              <p className="text-muted">{t(`home_card_Ertak.page_Title35`)} </p>
              <p className="text-muted">{t(`home_card_Ertak.page_Title36`)}</p>
            </div>
          </Col>
        </Row>

        <h3 className="font-weight-bold w-100 text-center mb-3">
          {" "}
          {t(`home_card_Ertak.page_Title37`)}{" "}
        </h3>

        <p className="text-muted">{t(`home_card_Ertak.page_Title38`)}</p>
        <br />
        <p className="text-muted">{t(`home_card_Ertak.page_Title39`)}</p>
      </div>
    </div>
  );
}

export default Ertaklar;
