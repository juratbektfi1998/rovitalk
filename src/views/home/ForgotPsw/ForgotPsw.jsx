import React from "react";
import { Formik } from "formik";

import * as Yup from "yup";

import "./ForgotPsw.scss";
import { useTranslation } from "react-i18next";

const ForgotSchema = Yup.object().shape({
  userName: Yup.string().required("required"),
});

function ForgotPsw() {
  const { t } = useTranslation();
  return (
    <div className="forgot-pws">
      <p className="text-muted"> {t(`home_main_Footer.pageTitle`)} </p>

      <Formik
        initialValues={{ userName: "" }}
        validationSchema={ForgotSchema}
        onSubmit={(values, { setSubmitting }) => {
          setTimeout(() => {
            alert(JSON.stringify(values, null, 2));
            setSubmitting(false);
          }, 400);
        }}
      >
        {({
          values,
          errors,
          touched,
          handleChange,
          handleBlur,
          handleSubmit,
          isSubmitting,
        }) => (
          <form onSubmit={handleSubmit} className="forgot-form">
            <div className="form-item mb-3">
              <label htmlFor="userName" className="text-muted">
                * {t(`home_main_Footer.pageTitle2`)}
              </label>
              <input
                type="text"
                id="userName"
                name="userName"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.userName}
                style={{
                  borderColor:
                    errors.userName && touched.userName ? "red" : "green",
                }}
              />
              <small className="text-danger font-weight-bolder">
                {" "}
                {errors.userName && touched.userName && errors.userName}{" "}
              </small>
            </div>

            <button
              type="submit"
              disabled={isSubmitting}
              className="form-btn shadow-sm"
            >
              {t(`home_main_Footer.pageTitle3`)}
            </button>
          </form>
        )}
      </Formik>

      {/* <Formik
        initialValues={{ email: "", password: "", saved: false }}
        onSubmit={(values, { setSubmitting }) => {
          setTimeout(() => {
            alert(JSON.stringify(values, null, 2));
            setSubmitting(false);
          }, 400);
        }}
      >
        {({
          values,
          errors,
          touched,
          handleChange,
          handleBlur,
          handleSubmit,
          isSubmitting,
      
        }) => (
          <form onSubmit={handleSubmit} className="login-form">
            <div className="form-item mb-3">
              <label htmlFor="email" className="text-muted">
                * Foydalanuvchi nomi
              </label>
              <input
                id="email"
                type="email"
                name="email"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.email}
              />
              {errors.email && touched.email && errors.email}
            </div>

            <div className="form-item mb-3">
              <label htmlFor="password" className="text-muted ">
                * Parol
              </label>
              <input
                id="password"
                type="password"
                name="password"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.password}
              />
              {errors.password && touched.password && errors.password}
            </div>

            <button
              type="submit"
              disabled={isSubmitting}
              className="form-btn shadow-sm"
            >
              Submit
            </button>
            <div className="mt-3">
              <p className="text-muted">Biz bilan bo'ling </p>
            </div>
          </form>
        )}
      </Formik> */}
    </div>
  );
}

export default ForgotPsw;
