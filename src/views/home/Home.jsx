import React from "react";
import { Route, Switch } from "react-router-dom";
import { Container } from "reactstrap";
import HomeFooter from "../../layouts/home/HomeFooter/HomeFooter";
import HomeNavbar from "../../layouts/home/HomeNavbar/HomeNavbar";

import MainPage from "./Main/MainPage";
import PricingPlans from "./PricingPlans/PricingPlans";
import "./Home.scss";
import GoTop from "../../components/GoTop/GoTop";
import LoginPage from "./LoginPage/LoginPage";
import SecurityPage from "./SecurityPage/SecurityPage";
import Rules from "./Rules/Rules";
import TitleHamlet from "../../components/TitleHamlet/TitleHamlet";
import Ertaklar from "./Ertaklar/Ertaklar";
import Meditatsiya from "./Meditatsiya/Meditatsiya";

function Home() {
  return (
    <div>
      <TitleHamlet title="Rovitalk" />{" "}
      <Container>
        <HomeNavbar />
        <main className="home-page-main">
          <Switch>
            <Route path={`/`} component={MainPage} exact />
            <Route path={`/main`} component={MainPage} />
            <Route path={`/login`} component={LoginPage} />
            <Route path={`/securtypage`} component={SecurityPage} />
            <Route path={`/rules`} component={Rules} />
            <Route path={`/tariflar`} component={PricingPlans} />
            <Route path={"/ertaklar"} component={Ertaklar} />

            <Route path={"/meditatsiya"} component={Meditatsiya} />

            <Route path="/*" render={() => <div>not found</div>} />

            {/* <Redirect to="/*" /> */}
            {/* page not found */}
          </Switch>
        </main>
      </Container>
      <GoTop />
      <HomeFooter />
    </div>
  );
}

export default Home;
