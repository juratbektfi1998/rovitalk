import React, { useState } from "react";

import { Formik } from "formik";

import * as Yup from "yup";
import "./Login.scss";
import { useHistory, useLocation } from "react-router-dom";
import ModalItem from "../../../components/ModalItem/ModalItem";
import ForgotPsw from "../ForgotPsw/ForgotPsw";
// import Axios from "axios";
import { useAuth } from "../../../components/Auth/Auth";
import Registration from "../Registration/Registration";
import { useTranslation } from "react-i18next";
import axios from '../../../config/axios-interceptor';
import { Input, Row, Col } from "reactstrap";



function Login() {

  const { t } = useTranslation();


  const LoginSchema = Yup.object().shape({
    phone_number: Yup.string().required(t(`Titul2`)),
  });




  const [verifi, setVerifi] = useState();
  const [number, setNumber] = useState(null);
  let history = useHistory();
  let location = useLocation();
  let auth = useAuth();
  let { from } = location.state || { from: { pathname: "/" } };
  let login = () => {
    auth.signin(() => {
      history.replace(from);
    });
  };

  const getSms = (phone_number, setSubmitting) => {
      axios.post('login', {
          phone_number
      }).then(res => {
          setSubmitting(false);
          setVerifi(true);
      }).catch(err => alert(err));
  };

  return (
    <div className="login-page py-5">
      <div className="mb-3">
        <p className="text-center w-100 text-muted">
          {t(`home_main_Login.pageTitle2`)}
        </p>
      </div>

      {/* "handleSubmit" will validate your inputs before invoking "onSubmit" */}

      

      <Formik
        initialValues={{
          prefix:"998",
          phone_number: "",
          verification_code: "",
         
        }}
        validationSchema={LoginSchema}
        onSubmit={(values, { setSubmitting }) => {

         
            // let phone_number;
            // if (values.phone_number.startsWith('+998')) {
            //     phone_number = values.phone_number.replace('+', '');
            // } else {
            //     phone_number = values.phone_number;
            // }
            // setNumber(values.phone_number);


            let prefix_number = values.prefix + values.phone_number;
        
            setNumber(prefix_number);
            
            if (!verifi) {
                getSms(prefix_number, setSubmitting)
            } else {
                axios.post('verify', {
                    phone_number: prefix_number,
                    verification_code: values.verification_code
                }).then(res => {
                    const token = res.data.data.token;
                    localStorage.setItem('rovi-authenticationToken', token);
                        axios.interceptors.request.use(function (config) {
                        config.headers.Authorization =  token;
                        return config;
                    });
                    login()
                })
                    .catch(err => alert(err))
            }
        }}
      >
        {({
          values,
          errors,
          touched,
          handleChange,
          handleBlur,
          handleSubmit,
          isSubmitting,
          /* and other goodies */
        }) =>  {   
          


          return (
            <form onSubmit={handleSubmit} className="login-form">

<label htmlFor="phone_number" className="text-muted">
                  * {t(`home_main_Login.pageTitle3`)}
                </label>



            <Row className=" w-100 justify-content-center" >
            
              <Col sm="3">


             <div className="mb-3">
             <Input type="select" className="form-select"
        name="prefix"
        value={values.prefix}
        onChange={handleChange}
        onBlur={handleBlur}
   
      > 
        <option  selected defaultValue="998" label="+998"   />
        <option value="65" label="+65" />
        <option value="21" label="+21" />
        <option value="85" label="+85" />
      </Input>
             </div>
                
              {/* <div><Input type="select"  className="mb-3"  name="prefix"
                  required
                  onChange={handleChange}
                  onBlur={handleBlur}
               
                  >
          <option selected defaultValue={"+998"} className="text-muted" >+998</option>
          <option value="+85" >+85</option>

          <option value="+36">+36</option>
          <option value="+12">+12</option>
        


        </Input></div> */}
              </Col>
              <Col sm="9">
              <div className="form-item mb-3">
              
                <input
                  id="phone_number"
                  type="text"
                  name="phone_number"
                  required
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.phone_number}
                  style={{
                    borderColor:
                      errors.phone_number && touched.phone_number
                        ? "red"
                        : "green",
                  }}
                />
                <small className="text-danger font-weight-bolder">
                  {" "}
                  {errors.phone_number &&
                    touched.phone_number &&
                    errors.phone_number}{" "}
                </small>
              </div>
              </Col>
            </Row>
              {verifi && (
                <div>
                <div className="form-item mb-3">
                  <label htmlFor="verification_code" className="text-muted">
                    * {t(`home_main_Login.pageTitle4`)}
                  </label>
                  <input
                    id="verification_code"
                    type="text"
                    name="verification_code"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    required
                    value={values.verification_code}
                    style={{
                      borderColor:
                        errors.verification_code && touched.verification_code
                          ? "red"
                          : "green",
                    }}
                  />
                  <small className="text-danger font-weight-bolder">
                    {" "}
                    {errors.verification_code &&
                      touched.verification_code &&
                      errors.verification_code}{" "}
                  </small>
  
                </div>
                <div className="right-box">
                  <p onClick={()=>getSms(number)}>Qayta sms yuborish</p>
                 </div>
                 </div>
               )}
               
              <button
                type="submit"
                disabled={isSubmitting}
                className="form-btn shadow-sm w-50"
              >
                {t(`home_main_Login.pageTitle`)}
              </button>
              <div className="mt-3">
                <p className="text-muted d-flex">
                  <span className="mr-2">{t(`home_main_Login.pageTitle8`)}</span>{" "}
                  <ModalItem
                    colorText="green"
                    buttonLabel={t(`home_main_Login.pageTitle9`)}
                    title={t(`home_main_Login.pageTitle9`)}
                    component={<Registration />}
                      btnClass="simple-text"
  
                  />
                  {/* <Link to="/dashboard/registration"> Ro'yxatdan o'tish </Link>{" "} */}
                </p>
              </div>
            </form>
            )

         } }
      </Formik>
    </div>
  );
    }
export default Login;
