import React, {useEffect} from "react";
import MainPageHeaderSlider from "../../../layouts/mainPage/MainPageHeaderSlider/MainPageHeaderSlider";
import MainSectionCounter from "../../../layouts/mainPage/MainsectionCounter/MainSectionCounter";
import MainSectionFive from "../../../layouts/mainPage/MainSectionFive/MainSectionFive";
import MainSectionFour from "../../../layouts/mainPage/MainSectionFour/MainSectionFour";
import MainSectionOne from "../../../layouts/mainPage/MainSectionOne/MainSectionOne";
import MainSectionPartners from "../../../layouts/mainPage/MainSectionPartners/MainSectionPartners";
import MainSectionReg from "../../../layouts/mainPage/MainSectionReg/MainSectionReg";
import MainSectionSix from "../../../layouts/mainPage/MainSectionSix/MainSectionSix";
import MainSectionThree from "../../../layouts/mainPage/MainSectionThree/MainSectionThree";
import MainSectionTwo from "../../../layouts/mainPage/MainSectionTwo/MainSectionTwo";

function MainPage() {

  useEffect(() => {
    window.scrollTo({ top: 0, behavior: "smooth" });
  }, []);

  return (
    <div className="mt-3" id="main-section">
      <MainPageHeaderSlider />
      <MainSectionOne />
      <MainSectionTwo />
      <MainSectionThree />
      <MainSectionFour />
      <MainSectionFive />
      <MainSectionSix />
      <MainSectionCounter />
      <MainSectionReg />
      <MainSectionPartners />
    </div>
  );
}

export default MainPage;
