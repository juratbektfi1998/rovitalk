import React, { useEffect } from "react";
import { useTranslation } from "react-i18next";
import { Col, Row } from "reactstrap";

function Meditatsiya() {
  useEffect(() => {
    window.scrollTo({ top: 0, behavior: "smooth" });
  }, []);
  const { t, i18n } = useTranslation();
  return (
    <div>
      <Row className="mt-5 mb-5">
        <Col className="text-center pt-5">
          <h1>{t(`home_card_Meditatsiya.page_Title`)}</h1>
        </Col>
      </Row>
      <Row>
        <Col className="text-center">
          <h3>{t(`home_card_Meditatsiya.page_Title1`)}</h3>
        </Col>
      </Row>
      <Row className="mt-3">
        <Col className="pt-5 pb-5">
          <p className="text-muted">{t(`home_card_Meditatsiya.page_Title2`)}</p>
          <p className="text-muted">{t(`home_card_Meditatsiya.page_Title3`)}</p>
          <p className="text-muted">{t(`home_card_Meditatsiya.page_Title4`)}</p>
        </Col>
      </Row>
      <Row>
        <Col className="text-center mt-5">
          <h3>{t(`home_card_Meditatsiya.page_Title5`)}</h3>
        </Col>
      </Row>
      <Row>
        <Col className="pt-4">
          <p className="text-muted">{t(`home_card_Meditatsiya.page_Title6`)}</p>
          <p className="font-weight-bold">
            {t(`home_card_Meditatsiya.page_Title7`)}
          </p>
          <p className="text-muted">{t(`home_card_Meditatsiya.page_Title8`)}</p>
          <p className="font-weight-bold">
            {t(`home_card_Meditatsiya.page_Title9`)}
          </p>
          <p className="text-muted">
            {t(`home_card_Meditatsiya.page_Title10`)}
          </p>
          <p className="font-weight-bold">
            {t(`home_card_Meditatsiya.page_Title11`)}
          </p>
          <p className="text-muted">
            {t(`home_card_Meditatsiya.page_Title12`)}
          </p>
          <p className="font-weight-bold">
            {t(`home_card_Meditatsiya.page_Title13`)}
          </p>
          <p className="text-muted">
            {t(`home_card_Meditatsiya.page_Title14`)}
          </p>
        </Col>
      </Row>
      <Row>
        <Col className="text-center mt-5">
          <h3>{t(`home_card_Meditatsiya.page_Title15`)}</h3>
        </Col>
      </Row>
      <Row>
        <Col className="pt-4">
          <p className="text-muted">
            {t(`home_card_Meditatsiya.page_Title16`)}
          </p>
          <p className="font-weight-bold">
            {t(`home_card_Meditatsiya.page_Title17`)}
          </p>
          <p className="text-muted">
            {t(`home_card_Meditatsiya.page_Title18`)}
          </p>
          <p className="font-weight-bold">
            {t(`home_card_Meditatsiya.page_Title19`)}
          </p>
          <p className="text-muted">
            {t(`home_card_Meditatsiya.page_Title20`)}
          </p>
          <p className="font-weight-bold">
            {t(`home_card_Meditatsiya.page_Title21`)}
          </p>
          <p className="text-muted">
            {t(`home_card_Meditatsiya.page_Title22`)}
          </p>
          <p className="text-muted">
            {t(`home_card_Meditatsiya.page_Title23`)}
          </p>
        </Col>
      </Row>
      <Row>
        <Col className="text-center mt-5">
          <h3>{t(`home_card_Meditatsiya.page_Title24`)}</h3>
        </Col>
      </Row>
      <Row>
        <Col className="pt-4">
          <p className="text-muted">
            {t(`home_card_Meditatsiya.page_Title25`)}
          </p>
          <p className="text-muted">
            {t(`home_card_Meditatsiya.page_Title26`)}
          </p>
          <p className="text-muted">
            <span className="font-weight-bold">
              {t(`home_card_Meditatsiya.page_Title27`)}
            </span>
            {t(`home_card_Meditatsiya.page_Title28`)}
          </p>
          <p className="font-weight-bold">
            {t(`home_card_Meditatsiya.page_Title29`)}
          </p>
          <p className="text-muted">
            {t(`home_card_Meditatsiya.page_Title30`)}
          </p>
          <p className="font-weight-bold">
            {t(`home_card_Meditatsiya.page_Title31`)}
          </p>
          <p className="text-muted">
            {t(`home_card_Meditatsiya.page_Title32`)}
          </p>
          <p className="font-weight-bold">
            {t(`home_card_Meditatsiya.page_Title33`)}
          </p>
          <p className="text-muted">
            {t(`home_card_Meditatsiya.page_Title34`)}
          </p>
        </Col>
      </Row>
      <Row>
        <Col className="text-center mt-5">
          <h3>{t(`home_card_Meditatsiya.page_Title35`)}</h3>
        </Col>
      </Row>
      <Row>
        <Col className="pt-4">
          <p className="text-muted">
            {t(`home_card_Meditatsiya.page_Title36`)}
          </p>
          <p className="text-muted">
            {t(`home_card_Meditatsiya.page_Title37`)}
          </p>
          <p className="text-muted">
            <span className="font-weight-bold">
              {t(`home_card_Meditatsiya.page_Title27`)}
            </span>
            {t(`home_card_Meditatsiya.page_Title38`)}
          </p>
          <p className="text-muted">
            {t(`home_card_Meditatsiya.page_Title39`)}
          </p>
        </Col>
      </Row>
    </div>
  );
}

export default Meditatsiya;
