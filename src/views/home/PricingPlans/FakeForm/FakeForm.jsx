import React, { useEffect, useState } from "react";
import { Formik } from "formik";
import Axios from "axios";
import { Button, Form, FormGroup, Label, Input, FormText } from "reactstrap";
function FakeForm() {
  const [options, setOptions] = useState();

  useEffect(() => {
    const fetchData = async () => {
      try {
        const result = await Axios(
          "https://online-market-piramida.herokuapp.com/plan/get-plans-info"
        );
        setOptions(result.data);
      } catch (err) {}
    };

    fetchData();
  }, []);

  return (
    <div>
      <Formik
        initialValues={{
          name: "",
          password: "",
          description: "",
          path: "",
          plan: "",
          type: "",
        }}
        onSubmit={(values, { setSubmitting }) => {
          setTimeout(() => {

            let formData = new FormData();

            formData.append("file", values.path);
            formData.append("upload_preset", "hti1irkx");

            // Axios(
            //   {
            //     method: "POST",
            //     baseURL: "https://api.cloudinary.com/",
            //     url: "v1_1/javohirdeveloper/image/upload",
            //     headers: {
            //       "Content-Type": `multipart/form-data`,
            //     },
            //   },
            //   formData
            // )
            //   .then((res) => {
            //     console.log(res);
            //   })
            //   .catch((err) => {
            //     console.log(err);
            //   } );

            fetch(
              `https://api.cloudinary.com/v1_1/javohirdeveloper/auto/upload`,
              {
                method: "POST",
                body: formData,
              }
            )
              .then((response) => {
                return response.text();
              })
              .then((data) => {
                data = JSON.parse(data);
                // console.log(data.url);
                values.path = data.url;
                alert(JSON.stringify(values, null, 2));
                setSubmitting(false);
              });

            // console.log(localStorage.getItem("auth-token"));

            Axios.put(
              "http://0eaa5c457fc5.ngrok.io/api/profile/15/",
              values.path,
              {
                headers: {
                  Authorization: `Token ${localStorage.getItem("auth-token")}`,
                  "Content-Type": "multipart/form-data",
                },
              }
            );

            // let form_data = new FormData();

            // form_data.append("image", values.path, values.path.name);
            // console.log(values.path.name);

            // let url = "http://0eaa5c457fc5.ngrok.io/api/profile/23/";

            // Axios.put(url, form_data, {
            //   headers: {
            //     "content-type": "multipart/form-data",
            //   },
            // })
            //   .then((res) => {
            //     console.log(res.data);
            //   })
            //   .catch((err) => console.log(err));
          }, 400);
        }}
      >
        {({
          values,
          handleChange,
          handleBlur,
          handleSubmit,
          isSubmitting,
          setFieldValue,
          /* and other goodies */
        }) => (
          <Form onSubmit={handleSubmit}>
            <FormGroup>
              <Label for="name">Name</Label>
              <Input
                id="name"
                type="text"
                name="name"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.name}
              />
            </FormGroup>

            <FormGroup>
              <Label for="description">description</Label>
              <Input
                id="description"
                type="text"
                name="description"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.description}
              />
            </FormGroup>
            {/* {errors.email && touched.email && errors.email} */}
            <FormGroup>
              <Label for="exampleFile">File</Label>
              <Input
                type="file"
                name="path"
                onChange={(event) => {
                  setFieldValue("path", event.currentTarget.files[0]);
                }}
                id="exampleFile"
              />
              <FormText color="muted">
                This is some placeholder block-level help text for the above
                input. It's a bit lighter and easily wraps to a new line.
              </FormText>
            </FormGroup>
            {/* {errors.password && touched.password && errors.password} */}

            {/* {errors.password && touched.password && errors.password} */}

            <FormGroup>
              <Label for="plan">Plan</Label>
              <Input
                type="select"
                name="plan"
                onChange={handleChange}
                onBlur={handleBlur}
                id="plan"
              >
                {options &&
                  options.map((item, index) => (
                    <option value={item._id} key={index}>
                      {" "}
                      {item.title}{" "}
                    </option>
                  ))}
              </Input>
            </FormGroup>
            <FormGroup>
              <Label for="type">Type</Label>
              <Input
                type="select"
                name="type"
                onChange={handleChange}
                onBlur={handleBlur}
                id="type"
              >
                <option value="auto" selected>
                  All
                </option>
                <option value="image"> Image </option>
                <option value="video"> video </option>
                <option value="audio"> audio </option>
                <option value="pdf">PDF </option>
              </Input>
            </FormGroup>

            <FormGroup>
              <Label for="password">password</Label>
              <Input
                id="password"
                type="password"
                name="password"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.password}
              />
            </FormGroup>

            <Button type="submit" disabled={isSubmitting}>
              Submit
            </Button>
          </Form>
        )}
      </Formik>
    </div>
  );
}

export default FakeForm;
