import React from "react";
import "./PricingCard.scss";

import ImgOne from "../../../../assets/pricingImages/Capture-removebg-preview.png";
import ImgTwo from "../../../../assets/pricingImages/Capture556555-removebg-preview.png";
import ImgThree from "../../../../assets/pricingImages/Capturesddjjd-removebg-preview.png";
import {Link} from "react-router-dom";
import ToolTip from "../../../../components/ToolTip/ToolTip";

function PricingCard({cardTitul, monthNumber, sum, dol, rubl, price}) {

    const addPrice = (price) => {
        const newPrice = {
            ...price,
            title: cardTitul,
            monthNumber
        };
        let prices = JSON.parse(localStorage.getItem('prices'));
        if (prices) {
            const check = prices.find(price => price['_id'] === newPrice['_id']);
            if (!check) {
                prices.push(newPrice);
            }
        } else {
            prices = [newPrice];
        }
        localStorage.setItem('prices', JSON.stringify(prices));
    };

    return (
        <div className="pricing-card ">
            <Link to={"/dashboard/singleprice/1"} className="card p-1 w-100 mb-3">
                <div className="title-card w-100 p-1 text-center text-uppercase font-weight-bold">
                    {cardTitul}
                </div>

                <div className="under-card d-flex w-100 justify-content-around">
                    <div
                        className="left-box d-flex flex-column justify-content-center align-content-center align-items-center">
                        <div className="number"> {monthNumber} </div>
                        <div className="under-text w-100 text-center">
                            {" "}
                            oylik <br/> tarif
                        </div>
                    </div>
                    <div className="right-box d-flex flex-column justify-content-around">
                        <div className="under-right-item border-bottom">
                            <p className="text-muted font-weight-bold">
                                {" "}
                                <img src={ImgOne} alt={cardTitul}/> {sum}
                            </p>
                            <p className="under-text-small text-muted">
                                {" "}
                                O'zbekiston Respublikasini sumi{" "}
                            </p>
                        </div>

                        <div className="under-right-item border-bottom">
                            <p className="text-muted font-weight-bold">
                                {" "}
                                <img src={ImgTwo} alt={cardTitul}/> {rubl}
                            </p>
                            <p className="under-text-small text-muted">
                                {" "}
                                O'zbekiston Respublikasini sumi{" "}
                            </p>
                        </div>
                        <div className="under-right-item border-bottom">
                            <p className="text-muted font-weight-bold">
                                {" "}
                                <img src={ImgThree} alt={cardTitul}/> {dol}
                            </p>
                            <p className="under-text-small text-muted">
                                {" "}
                                O'zbekiston Respublikasini sumi{" "}
                            </p>
                        </div>
                    </div>
                </div>
            </Link>
            <h5 className="w-100 text-center font-weight-bold">
                <Link to={"/dashboard/singleprice/:id"} className="under-link">
                    {" "}
                    {cardTitul} {monthNumber} oylik tarif{" "}
                </Link>
            </h5>
            <p className="text-uppercase under-val w-100 text-center">
                100000 uzs <i id="summa" className="fas fa-info-circle"></i>{" "}
                <ToolTip
                    orentation="right"
                    customId="summa"
                    text="USD: 100 "
                    textUnder="RUBL: 200"
                />
            </p>
            <Link to={`/dashboard/checkout/${price._id}`} onClick={() => addPrice(price)} className="btn-link">
                {" "}
                Savatcha{" "}
            </Link>
        </div>
    );
}

export default PricingCard;
