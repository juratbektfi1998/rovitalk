import React, {useState, useEffect} from "react";
import {Col, Row} from "reactstrap";
import PricingCard from "./PricingCard/PricingCard";
import axios from "../../../config/axios-interceptor";
import "./PricingPlans.scss";
// import ModalItem from "../../../components/ModalItem/ModalItem";
// import FakeForm from "./FakeForm/FakeForm";

function PricingPlans() {
    const [data, setData] = useState([]);
    const [isLoading, setIsLoading] = useState(false);
    const [isError, setIsError] = useState(false);

    // const cardData = [
    //   {
    //     cardTitul: "Audiokitoblar",
    //     monthNumber: "1",
    //     sum: "100000",
    //     dol: "700",
    //     rubl: "10",
    //     korPath: "/dashboard/checkout/1",
    //     cardPath: "/dashboard/singleprice/1",
    //   },
    //   {
    //     cardTitul: "Audiokitoblar",
    //     monthNumber: "3",
    //     sum: "200000",
    //     dol: "1400",
    //     rubl: "20",
    //     korPath: "/dashboard/checkout/1",
    //     cardPath: "/dashboard/singleprice/1",
    //   },
    //   {
    //     cardTitul: "Audiokitoblar",
    //     monthNumber: "6",
    //     sum: "400000",
    //     dol: "2800",
    //     rubl: "40",
    //     korPath: "/dashboard/checkout/1",
    //     cardPath: "/dashboard/singleprice/1",
    //   },
    //   {
    //     cardTitul: "Ertakterapiya",
    //     monthNumber: "1",
    //     sum: "50000",
    //     dol: "400",
    //     rubl: "6",
    //     korPath: "/dashboard/checkout/1",
    //     cardPath: "/dashboard/singleprice/1",
    //   },
    //   {
    //     cardTitul: "Ertakterapiya",
    //     monthNumber: "3",
    //     sum: "100000",
    //     dol: "800",
    //     rubl: "12",
    //     korPath: "/dashboard/checkout/1",
    //     cardPath: "/dashboard/singleprice/1",
    //   },
    //   {
    //     cardTitul: "Ertakterapiya",
    //     monthNumber: "6",
    //     sum: "200000",
    //     dol: "1600",
    //     rubl: "25",
    //     korPath: "/dashboard/checkout/1",
    //     cardPath: "/dashboard/singleprice/1",
    //   },
    //   {
    //     cardTitul: "Hikmatlar",
    //     monthNumber: "1",
    //     sum: "100000",
    //     dol: "700",
    //     rubl: "10",
    //     korPath: "/dashboard/checkout/1",
    //     cardPath: "/dashboard/singleprice/1",
    //   },
    //   {
    //     cardTitul: "Hikmatlar",
    //     monthNumber: "3",
    //     sum: "200000",
    //     dol: "1400",
    //     rubl: "20",
    //     korPath: "/dashboard/checkout/1",
    //     cardPath: "/dashboard/singleprice/1",
    //   },
    //   {
    //     cardTitul: "Hikmatlar",
    //     monthNumber: "6",
    //     sum: "400000",
    //     dol: "2800",
    //     rubl: "40",
    //     korPath: "/dashboard/checkout/1",
    //     cardPath: "/dashboard/singleprice/1",
    //   },
    //   {
    //     cardTitul: "Meditatsiya",
    //     monthNumber: "1",
    //     sum: "200000",
    //     dol: "1400",
    //     rubl: "20",
    //     korPath: "/dashboard/checkout/1",
    //     cardPath: "/dashboard/singleprice/1",
    //   },
    //   {
    //     cardTitul: "Meditatsiya",
    //     monthNumber: "3",
    //     sum: "400000",
    //     dol: "2800",
    //     rubl: "40",
    //     korPath: "/dashboard/checkout/1",
    //     cardPath: "/dashboard/singleprice/1",
    //   },
    //   {
    //     cardTitul: "Meditatsiya",
    //     monthNumber: "6",
    //     sum: "800000",
    //     dol: "5600",
    //     rubl: "80",
    //     korPath: "/dashboard/checkout/1",
    //     cardPath: "/dashboard/singleprice/1",
    //   },
    // ];

    useEffect(() => {
        axios.post("prices")
            .then(res => setData(res.data.data));
    }, []);

    const cardData = [];

    const convert = () => {
        data.map(item => {
            item.prices.map(price => {
                const newItem = {
                    cardTitul: item.title,
                    monthNumber: price.duration,
                    sum: price.cost,
                    dol: "5600",
                    rubl: "80",
                    korPath: "/dashboard/checkout/1",
                    cardPath: "/dashboard/singleprice/1",
                    price: price
                };
                cardData.push(newItem)
            })
        })
    };

    convert();

    return (
        <div className="pricing-plans">
            <h1 className="my-5 w-100 text-center font-weight-bold">Tarif Rejalar</h1>
            <div className="mb-3">
                <h3 className="font-weight-bold text-muted w-100 text-center">
                    E'tibor bilan o'zingizga kerakli bo'lgan ta'rifni tanlang.
                </h3>
                <p className="text-center text-muted w-100">
                    Ta'rifni tangan vaqtingizda, diqqat bilan har bir ma'lumotga e'tibor
                    bering. Chunki to'lov ketmaketlikda amalga oshiriladi.
                </p>
            </div>
            <Row>
                <Col sm="12" md={{size: 8, offset: 2}}>
                    <Row className="flex-wrap">
                        {cardData.map((card, index) => (
                            <Col sm="12" md="6" lg="4" className="mb-3">
                                <PricingCard
                                    key={index}
                                    cardTitul={card.cardTitul}
                                    monthNumber={card.monthNumber}
                                    sum={card.sum}
                                    dol={card.dol}
                                    rubl={card.rubl}
                                    korPath={card.korPath}
                                    cardPath={card.cardPath}
                                    price={card.price}
                                />
                            </Col>
                        ))}
                    </Row>
                </Col>
            </Row>
            {/* <br />
      <Button>
        {" "}
        <ModalItem
          buttonLabel="show fakeForm"
          title="fakeForm"
          component={<FakeForm />}
        />{" "}
      </Button>

      {isError && <div>Something went wrong ...</div>}
      {isLoading ? (
        <div>Loading ...</div>
      ) : (
        <ul>
          {data &&
            data.map((item, index) => (
              <li key={index}>
                {" "}
                {item.description}{" "}
                <img
                  src={item.path}
                  style={{ width: "100px", height: "auto" }}
                  alt={item.name}
                />{" "}
              </li>
            ))}
        </ul>
      )} */}
        </div>
    );
}

export default PricingPlans;
