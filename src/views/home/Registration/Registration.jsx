import React, { useState } from "react";
import axios from "../../../config/axios-interceptor";
import { Formik } from "formik";

import * as Yup from "yup";

import "./Registration.scss";
import { useTranslation } from "react-i18next";
const LoginSchema = Yup.object().shape({
  phone_number: Yup.string().required("Telefon raqam"),
  first_name: Yup.string().required("Ism"),
  last_name: Yup.string().required("Familiya"),
});

function Registration() {
  const [verifi, setVerifi] = useState();
  const { t } = useTranslation();

  return (
    <div className="login-page py-5">
     

      {/* "handleSubmit" will validate your inputs before invoking "onSubmit" */}

      <Formik
        initialValues={{
          phone_number: "",
          first_name: "",
          last_name: "",
        }}
        validationSchema={LoginSchema}
        onSubmit={(values, { setSubmitting }) => {
          let phone_number;
          if (values.phone_number.startsWith("+998")) {
            phone_number = values.phone_number.replace("+998", "");
          }
          if (!verifi) {
            axios
              .post("/register", {
                first_name: values.first_name,
                last_name: values.last_name,
                phone_number,
              })
              .then((res) => {
                setSubmitting(false);
                setVerifi(true);
              })
              .catch((err) => alert(err));
          } else {
            axios
              .post("verify", {
                phone_number,
                verification_code: values.verification_code,
              })
              .then((res) => {
                alert(res.data);
                localStorage.setItem(
                  "rovi-authenticationToken",
                  res.data.data.token
                );
              })
              .catch((err) => alert(err));
          }
        }}
      >
        {({
          values,
          errors,
          touched,
          handleChange,
          handleBlur,
          handleSubmit,
          isSubmitting,
        }) => (
          <form onSubmit={handleSubmit} className="login-form">
            <div className="form-item mb-3">
              <label htmlFor="first_name" className="text-muted">
                * {t(`home_main_Login.pageTitle2`)}
              </label>
              <input
                id="first_name"
                type="text"
                name="first_name"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.first_name}
                style={{
                  borderColor:
                    errors.first_name && touched.first_name ? "red" : "green",
                }}
              />
              <small className="text-danger font-weight-bolder">
                {" "}
                {errors.first_name &&
                  touched.first_name &&
                  errors.first_name}{" "}
              </small>
            </div>
            <div className="form-item mb-3">
              <label htmlFor="last_name" className="text-muted">
                * {t(`home_main_Login.pageTitle3`)}
              </label>
              <input
                id="last_name"
                type="text"
                name="last_name"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.last_name}
                style={{
                  borderColor:
                    errors.last_name && touched.last_name ? "red" : "green",
                }}
              />
              <small className="text-danger font-weight-bolder">
                {" "}
                {errors.last_name && touched.last_name && errors.last_name}{" "}
              </small>
            </div>
            <div className="form-item mb-3">
              <label htmlFor="phone_number" className="text-muted">
                * {t(`home_main_Login.pageTitle5`)}
              </label>
              <input
                id="phone_number"
                type="text"
                name="phone_number"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.phone_number}
                style={{
                  borderColor:
                    errors.phone_number && touched.phone_number
                      ? "red"
                      : "green",
                }}
              />
              <small className="text-danger font-weight-bolder">
                {" "}
                {errors.phone_number &&
                  touched.phone_number &&
                  errors.phone_number}{" "}
              </small>
            </div>

            {verifi && (
              <div className="form-item mb-3">
                <label htmlFor="verification_code" className="text-muted">
                  * {t(`home_main_Login.pageTitle6`)}
                </label>
                <input
                  id="verification_code"
                  type="text"
                  name="verification_code"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.verification_code}
                  style={{
                    borderColor:
                      errors.verification_code && touched.verification_code
                        ? "red"
                        : "green",
                  }}
                />
                <small className="text-danger font-weight-bolder">
                  {" "}
                  {errors.verification_code &&
                    touched.verification_code &&
                    errors.verification_code}{" "}
                </small>
              </div>
            )}

            <button
              type="submit"
              disabled={isSubmitting}
              className="form-btn shadow-sm w-50"
            >
              {t(`home_main_Login.pageTitle`)}
            </button>
          </form>
        )}
      </Formik>
    </div>
  );
}

export default Registration;
